(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Pega todos os formulários que nós queremos aplicar estilos de validação Bootstrap personalizados.
    var forms = document.getElementsByClassName('needs-validation');
    // Faz um loop neles e evita o envio
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
          
        }
        form.classList.add('was-validated');
      }, false);
      
    });
  }, false);
  


  $('#cpf').mask('000.000.000-00', {reverse: true});
  $('#inputCPF-Certificado-Digital').mask('000.000.000-00', {reverse: true});
  $('#inputCPF-Contribuicoes').mask('000.000.000-00', {reverse: true});

  $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
  $('#inputDocumentoPerfil').mask('00.000.000/0000-00', {reverse: true});
  $('#inputCNPJ-Certificado-Digital').mask('00.000.000/0000-00', {reverse: true});
  $('#inputCNPJ-Contribuicoes').mask('00.000.000/0000-00', {reverse: true});
  
  $('#inputCNPJCO').mask('00.000.000/0000-00', {reverse: true});
  $('#inputCPF-Certificado-Digital').mask('000.000.000-00', {reverse: true});
  $('#inputCNPJ-Certificado-Digital').mask('00.000.000/0000-00', {reverse: true});
  $('#inputPhoneBRCD').mask('(99)9 9999-9999');
  $('#inputPhoneBRs').mask('(99)9 9999-9999');
  $('#inputPhoneBR-Contribuicoes').mask('(99)9 9999-9999');
  
  $("#cpf").addClass("inputaparece");
  $("#inputCPF-Certificado-Digital").addClass("inputaparece");
  $("#inputCPF-Contribuicoes").addClass("inputaparece");
  
  $("#inputNome-Certificado-Digital").addClass("inputaparece");
  $("#inputNome-Contribuicoes").addClass("inputaparece");
  
  $("#cnpj").addClass("inputdesaparece");
  $("#inputCNPJ-Certificado-Digital").addClass("inputdesaparece");
  $("#inputCNPJ-Contribuicoes").addClass("inputdesaparece");

  $("#inputNomeFantasia-Certificado-Digital").addClass("inputdesaparece");
  $("#inputNomeFantasia-Contribuicoes").addClass("inputdesaparece");
  document.getElementById("cpf").required = true;
  document.getElementById("cnpj").required = false;
  document.getElementById("inputCPF-Certificado-Digital").required = true;
  document.getElementById("inputNome-Certificado-Digital").required = true;
  document.getElementById("inputCPF-Contribuicoes").required = true;
  document.getElementById("inputNome-Contribuicoes").required = true;

  document.getElementById("inputCNPJ-Certificado-Digital").required = false;
  document.getElementById("inputNomeFantasia-Certificado-Digital").required = false;
  document.getElementById("inputCNPJ-Contribuicoes").required = false;
  document.getElementById("inputNomeFantasia-Contribuicoes").required = false;
 
  AOS.init({disable :  false});

})();



