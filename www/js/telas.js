const date_picker_element = document.querySelector('.date-picker');
const selected_date_element = document.querySelector('.date-picker .selected-date');
const dates_element = document.querySelector('.date-picker .dates');
const mth_element = document.querySelector('.date-picker .dates .month .mth');
const next_mth_element = document.querySelector('.date-picker .dates .month .next-mth');
const prev_mth_element = document.querySelector('.date-picker .dates .month .prev-mth');
const days_element = document.querySelector('.date-picker .dates .days');
var datas= [];
var vagas = [];
var vagasCandidatos = [];
var pesquisaVagas = [];
var vagasCandidatosPesquisa = [];
var datasEscolhidas= [];
var eventos;
var convenios;
var solicitacoes;
var salas;
var convencoesPdf;
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

(function (global) {
  'use strict';

  var util = newUtil();
  var inliner = newInliner();
  var fontFaces = newFontFaces();
  var images = newImages();

  // Default impl optionsbtnTelaPerfil
  var defaultOptions = {
      // Default is to fail on error, no placeholder
      imagePlaceholder: undefined,
      // Default cache bust is false, it will use the cache
      cacheBust: false
  };

  var domtoimage = {
      toSvg: toSvg,
      toPng: toPng,
      toJpeg: toJpeg,
      toBlob: toBlob,
      toPixelData: toPixelData,
      impl: {
          fontFaces: fontFaces,
          images: images,
          util: util,
          inliner: inliner,
          options: {}
      }
  };

  if (typeof module !== 'undefined')
      module.exports = domtoimage;
  else
      global.domtoimage = domtoimage;


  /**
   * @param {Node} node - The DOM Node object to render
   * @param {Object} options - Rendering options
   * @param {Function} options.filter - Should return true if passed node should be included in the output
   *          (excluding node means excluding it's children as well). Not called on the root node.
   * @param {String} options.bgcolor - color for the background, any valid CSS color value.
   * @param {Number} options.width - width to be applied to node before rendering.
   * @param {Number} options.height - height to be applied to node before rendering.
   * @param {Object} options.style - an object whose properties to be copied to node's style before rendering.
   * @param {Number} options.quality - a Number between 0 and 1 indicating image quality (applicable to JPEG only),
              defaults to 1.0.
   * @param {String} options.imagePlaceholder - dataURL to use as a placeholder for failed images, default behaviour is to fail fast on images we can't fetch
   * @param {Boolean} options.cacheBust - set to true to cache bust by appending the time to the request url
   * @return {Promise} - A promise that is fulfilled with a SVG image data URL
   * */
  function toSvg(node, options) {
      options = options || {};
      copyOptions(options);
      return Promise.resolve(node)
          .then(function (node) {
              return cloneNode(node, options.filter, true);
          })
          .then(embedFonts)
          .then(inlineImages)
          .then(applyOptions)
          .then(function (clone) {
              return makeSvgDataUri(clone,
                  options.width || util.width(node),
                  options.height || util.height(node)
              );
          });

      function applyOptions(clone) {
          if (options.bgcolor) clone.style.backgroundColor = options.bgcolor;

          if (options.width) clone.style.width = options.width + 'px';
          if (options.height) clone.style.height = options.height + 'px';

          if (options.style)
              Object.keys(options.style).forEach(function (property) {
                  clone.style[property] = options.style[property];
              });

          return clone;
      }
  }

  /**
   * @param {Node} node - The DOM Node object to render
   * @param {Object} options - Rendering options, @see {@link toSvg}
   * @return {Promise} - A promise that is fulfilled with a Uint8Array containing RGBA pixel data.
   * */
  function toPixelData(node, options) {
      return draw(node, options || {})
          .then(function (canvas) {
              return canvas.getContext('2d').getImageData(
                  0,
                  0,
                  util.width(node),
                  util.height(node)
              ).data;
          });
  }

  /**
   * @param {Node} node - The DOM Node object to render
   * @param {Object} options - Rendering options, @see {@link toSvg}
   * @return {Promise} - A promise that is fulfilled with a PNG image data URL
   * */
  function toPng(node, options) {
      return draw(node, options || {})
          .then(function (canvas) {
              return canvas.toDataURL();
          });
  }

  /**
   * @param {Node} node - The DOM Node object to render
   * @param {Object} options - Rendering options, @see {@link toSvg}
   * @return {Promise} - A promise that is fulfilled with a JPEG image data URL
   * */
  function toJpeg(node, options) {
      options = options || {};
      return draw(node, options)
          .then(function (canvas) {
              return canvas.toDataURL('image/jpeg', options.quality || 1.0);
          });
  }

  /**
   * @param {Node} node - The DOM Node object to render
   * @param {Object} options - Rendering options, @see {@link toSvg}
   * @return {Promise} - A promise that is fulfilled with a PNG image blob
   * */
  function toBlob(node, options) {
      return draw(node, options || {})
          .then(util.canvasToBlob);
  }

  function copyOptions(options) {
      // Copy options to impl options for use in impl
      if(typeof(options.imagePlaceholder) === 'undefined') {
          domtoimage.impl.options.imagePlaceholder = defaultOptions.imagePlaceholder;
      } else {
          domtoimage.impl.options.imagePlaceholder = options.imagePlaceholder;
      }

      if(typeof(options.cacheBust) === 'undefined') {
          domtoimage.impl.options.cacheBust = defaultOptions.cacheBust;
      } else {
          domtoimage.impl.options.cacheBust = options.cacheBust;
      }
  }

  function draw(domNode, options) {
      return toSvg(domNode, options)
          .then(util.makeImage)
          .then(util.delay(100))
          .then(function (image) {
              var canvas = newCanvas(domNode);
              canvas.getContext('2d').drawImage(image, 0, 0);
              return canvas;
          });

      function newCanvas(domNode) {
          var canvas = document.createElement('canvas');
          canvas.width = options.width || util.width(domNode);
          canvas.height = options.height || util.height(domNode);

          if (options.bgcolor) {
              var ctx = canvas.getContext('2d');
              ctx.fillStyle = options.bgcolor;
              ctx.fillRect(0, 0, canvas.width, canvas.height);
          }

          return canvas;
      }
  }

  function cloneNode(node, filter, root) {
      if (!root && filter && !filter(node)) return Promise.resolve();

      return Promise.resolve(node)
          .then(makeNodeCopy)
          .then(function (clone) {
              return cloneChildren(node, clone, filter);
          })
          .then(function (clone) {
              return processClone(node, clone);
          });

      function makeNodeCopy(node) {
          if (node instanceof HTMLCanvasElement) return util.makeImage(node.toDataURL());
          return node.cloneNode(false);
      }

      function cloneChildren(original, clone, filter) {
          var children = original.childNodes;
          if (children.length === 0) return Promise.resolve(clone);

          return cloneChildrenInOrder(clone, util.asArray(children), filter)
              .then(function () {
                  return clone;
              });

          function cloneChildrenInOrder(parent, children, filter) {
              var done = Promise.resolve();
              children.forEach(function (child) {
                  done = done
                      .then(function () {
                          return cloneNode(child, filter);
                      })
                      .then(function (childClone) {
                          if (childClone) parent.appendChild(childClone);
                      });
              });
              return done;
          }
      }

      function processClone(original, clone) {
          if (!(clone instanceof Element)) return clone;

          return Promise.resolve()
              .then(cloneStyle)
              .then(clonePseudoElements)
              .then(copyUserInput)
              .then(fixSvg)
              .then(function () {
                  return clone;
              });

          function cloneStyle() {
              copyStyle(window.getComputedStyle(original), clone.style);

              function copyStyle(source, target) {
                  if (source.cssText) target.cssText = source.cssText;
                  else copyProperties(source, target);

                  function copyProperties(source, target) {
                      util.asArray(source).forEach(function (name) {
                          target.setProperty(
                              name,
                              source.getPropertyValue(name),
                              source.getPropertyPriority(name)
                          );
                      });
                  }
              }
          }

          function clonePseudoElements() {
              [':before', ':after'].forEach(function (element) {
                  clonePseudoElement(element);
              });

              function clonePseudoElement(element) {
                  var style = window.getComputedStyle(original, element);
                  var content = style.getPropertyValue('content');

                  if (content === '' || content === 'none') return;

                  var className = util.uid();
                  clone.className = clone.className + ' ' + className;
                  var styleElement = document.createElement('style');
                  styleElement.appendChild(formatPseudoElementStyle(className, element, style));
                  clone.appendChild(styleElement);

                  function formatPseudoElementStyle(className, element, style) {
                      var selector = '.' + className + ':' + element;
                      var cssText = style.cssText ? formatCssText(style) : formatCssProperties(style);
                      return document.createTextNode(selector + '{' + cssText + '}');

                      function formatCssText(style) {
                          var content = style.getPropertyValue('content');
                          return style.cssText + ' content: ' + content + ';';
                      }

                      function formatCssProperties(style) {

                          return util.asArray(style)
                              .map(formatProperty)
                              .join('; ') + ';';

                          function formatProperty(name) {
                              return name + ': ' +
                                  style.getPropertyValue(name) +
                                  (style.getPropertyPriority(name) ? ' !important' : '');
                          }
                      }
                  }
              }
          }

          function copyUserInput() {
              if (original instanceof HTMLTextAreaElement) clone.innerHTML = original.value;
              if (original instanceof HTMLInputElement) clone.setAttribute("value", original.value);
          }

          function fixSvg() {
              if (!(clone instanceof SVGElement)) return;
              clone.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

              if (!(clone instanceof SVGRectElement)) return;
              ['width', 'height'].forEach(function (attribute) {
                  var value = clone.getAttribute(attribute);
                  if (!value) return;

                  clone.style.setProperty(attribute, value);
              });
          }
      }
  }

  function embedFonts(node) {
      return fontFaces.resolveAll()
          .then(function (cssText) {
              var styleNode = document.createElement('style');
              node.appendChild(styleNode);
              styleNode.appendChild(document.createTextNode(cssText));
              return node;
          });
  }

  function inlineImages(node) {
      return images.inlineAll(node)
          .then(function () {
              return node;
          });
  }

  function makeSvgDataUri(node, width, height) {
      return Promise.resolve(node)
          .then(function (node) {
              node.setAttribute('xmlns', 'http://www.w3.org/1999/xhtml');
              return new XMLSerializer().serializeToString(node);
          })
          .then(util.escapeXhtml)
          .then(function (xhtml) {
              return '<foreignObject x="0" y="0" width="100%" height="100%">' + xhtml + '</foreignObject>';
          })
          .then(function (foreignObject) {
              return '<svg xmlns="http://www.w3.org/2000/svg" width="' + width + '" height="' + height + '">' +
                  foreignObject + '</svg>';
          })
          .then(function (svg) {
              return 'data:image/svg+xml;charset=utf-8,' + svg;
          });
  }

  function newUtil() {
      return {
          escape: escape,
          parseExtension: parseExtension,
          mimeType: mimeType,
          dataAsUrl: dataAsUrl,
          isDataUrl: isDataUrl,
          canvasToBlob: canvasToBlob,
          resolveUrl: resolveUrl,
          getAndEncode: getAndEncode,
          uid: uid(),
          delay: delay,
          asArray: asArray,
          escapeXhtml: escapeXhtml,
          makeImage: makeImage,
          width: width,
          height: height
      };

      function mimes() {
          /*
           * Only WOFF and EOT mime types for fonts are 'real'
           * see http://www.iana.org/assignments/media-types/media-types.xhtml
           */
          var WOFF = 'application/font-woff';
          var JPEG = 'image/jpeg';

          return {
              'woff': WOFF,
              'woff2': WOFF,
              'ttf': 'application/font-truetype',
              'eot': 'application/vnd.ms-fontobject',
              'png': 'image/png',
              'jpg': JPEG,
              'jpeg': JPEG,
              'gif': 'image/gif',
              'tiff': 'image/tiff',
              'svg': 'image/svg+xml'
          };
      }

      function parseExtension(url) {
          var match = /\.([^\.\/]*?)$/g.exec(url);
          if (match) return match[1];
          else return '';
      }

      function mimeType(url) {
          var extension = parseExtension(url).toLowerCase();
          return mimes()[extension] || '';
      }

      function isDataUrl(url) {
          return url.search(/^(data:)/) !== -1;
      }

      function toBlob(canvas) {
          return new Promise(function (resolve) {
              var binaryString = window.atob(canvas.toDataURL().split(',')[1]);
              var length = binaryString.length;
              var binaryArray = new Uint8Array(length);

              for (var i = 0; i < length; i++)
                  binaryArray[i] = binaryString.charCodeAt(i);

              resolve(new Blob([binaryArray], {
                  type: 'image/png'
              }));
          });
      }

      function canvasToBlob(canvas) {
          if (canvas.toBlob)
              return new Promise(function (resolve) {
                  canvas.toBlob(resolve);
              });

          return toBlob(canvas);
      }

      function resolveUrl(url, baseUrl) {
          var doc = document.implementation.createHTMLDocument();
          var base = doc.createElement('base');
          doc.head.appendChild(base);
          var a = doc.createElement('a');
          doc.body.appendChild(a);
          base.href = baseUrl;
          a.href = url;
          return a.href;
      }

      function uid() {
          var index = 0;

          return function () {
              return 'u' + fourRandomChars() + index++;

              function fourRandomChars() {
                  /* see http://stackoverflow.com/a/6248722/2519373 */
                  return ('0000' + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
              }
          };
      }

      function makeImage(uri) {
          return new Promise(function (resolve, reject) {
              var image = new Image();
              image.onload = function () {
                  resolve(image);
              };
              image.onerror = reject;
              image.src = uri;
          });
      }

      function getAndEncode(url) {
          var TIMEOUT = 30000;
          if(domtoimage.impl.options.cacheBust) {
              // Cache bypass so we dont have CORS issues with cached images
              // Source: https://developer.mozilla.org/en/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest#Bypassing_the_cache
              url += ((/\?/).test(url) ? "&" : "?") + (new Date()).getTime();
          }

          return new Promise(function (resolve) {
              var request = new XMLHttpRequest();

              request.onreadystatechange = done;
              request.ontimeout = timeout;
              request.responseType = 'blob';
              request.timeout = TIMEOUT;
              request.open('GET', url, true);
              request.send();

              var placeholder;
              if(domtoimage.impl.options.imagePlaceholder) {
                  var split = domtoimage.impl.options.imagePlaceholder.split(/,/);
                  if(split && split[1]) {
                      placeholder = split[1];
                  }
              }

              function done() {
                  if (request.readyState !== 4) return;

                  if (request.status !== 200) {
                      if(placeholder) {
                          resolve(placeholder);
                      } else {
                          fail('cannot fetch resource: ' + url + ', status: ' + request.status);
                      }

                      return;
                  }

                  var encoder = new FileReader();
                  encoder.onloadend = function () {
                      var content = encoder.result.split(/,/)[1];
                      resolve(content);
                  };
                  encoder.readAsDataURL(request.response);
              }

              function timeout() {
                  if(placeholder) {
                      resolve(placeholder);
                  } else {
                      fail('timeout of ' + TIMEOUT + 'ms occured while fetching resource: ' + url);
                  }
              }

              function fail(message) {
                  console.error(message);
                  resolve('');
              }
          });
      }

      function dataAsUrl(content, type) {
          return 'data:' + type + ';base64,' + content;
      }

      function escape(string) {
          return string.replace(/([.*+?^${}()|\[\]\/\\])/g, '\\$1');
      }

      function delay(ms) {
          return function (arg) {
              return new Promise(function (resolve) {
                  setTimeout(function () {
                      resolve(arg);
                  }, ms);
              });
          };
      }

      function asArray(arrayLike) {
          var array = [];
          var length = arrayLike.length;
          for (var i = 0; i < length; i++) array.push(arrayLike[i]);
          return array;
      }

      function escapeXhtml(string) {
          return string.replace(/#/g, '%23').replace(/\n/g, '%0A');
      }

      function width(node) {
          var leftBorder = px(node, 'border-left-width');
          var rightBorder = px(node, 'border-right-width');
          return node.scrollWidth + leftBorder + rightBorder;
      }

      function height(node) {
          var topBorder = px(node, 'border-top-width');
          var bottomBorder = px(node, 'border-bottom-width');
          return node.scrollHeight + topBorder + bottomBorder;
      }

      function px(node, styleProperty) {
          var value = window.getComputedStyle(node).getPropertyValue(styleProperty);
          return parseFloat(value.replace('px', ''));
      }
  }

  function newInliner() {
      var URL_REGEX = /url\(['"]?([^'"]+?)['"]?\)/g;

      return {
          inlineAll: inlineAll,
          shouldProcess: shouldProcess,
          impl: {
              readUrls: readUrls,
              inline: inline
          }
      };

      function shouldProcess(string) {
          return string.search(URL_REGEX) !== -1;
      }

      function readUrls(string) {
          var result = [];
          var match;
          while ((match = URL_REGEX.exec(string)) !== null) {
              result.push(match[1]);
          }
          return result.filter(function (url) {
              return !util.isDataUrl(url);
          });
      }

      function inline(string, url, baseUrl, get) {
          return Promise.resolve(url)
              .then(function (url) {
                  return baseUrl ? util.resolveUrl(url, baseUrl) : url;
              })
              .then(get || util.getAndEncode)
              .then(function (data) {
                  return util.dataAsUrl(data, util.mimeType(url));
              })
              .then(function (dataUrl) {
                  return string.replace(urlAsRegex(url), '$1' + dataUrl + '$3');
              });

          function urlAsRegex(url) {
              return new RegExp('(url\\([\'"]?)(' + util.escape(url) + ')([\'"]?\\))', 'g');
          }
      }

      function inlineAll(string, baseUrl, get) {
          if (nothingToInline()) return Promise.resolve(string);

          return Promise.resolve(string)
              .then(readUrls)
              .then(function (urls) {
                  var done = Promise.resolve(string);
                  urls.forEach(function (url) {
                      done = done.then(function (string) {
                          return inline(string, url, baseUrl, get);
                      });
                  });
                  return done;
              });

          function nothingToInline() {
              return !shouldProcess(string);
          }
      }
  }

  function newFontFaces() {
      return {
          resolveAll: resolveAll,
          impl: {
              readAll: readAll
          }
      };

      function resolveAll() {
          return readAll(document)
              .then(function (webFonts) {
                  return Promise.all(
                      webFonts.map(function (webFont) {
                          return webFont.resolve();
                      })
                  );
              })
              .then(function (cssStrings) {
                  return cssStrings.join('\n');
              });
      }

      function readAll() {
          return Promise.resolve(util.asArray(document.styleSheets))
              .then(getCssRules)
              .then(selectWebFontRules)
              .then(function (rules) {
                  return rules.map(newWebFont);
              });

          function selectWebFontRules(cssRules) {
              return cssRules
                  .filter(function (rule) {
                      return rule.type === CSSRule.FONT_FACE_RULE;
                  })
                  .filter(function (rule) {
                      return inliner.shouldProcess(rule.style.getPropertyValue('src'));
                  });
          }

          function getCssRules(styleSheets) {
              var cssRules = [];
              styleSheets.forEach(function (sheet) {
                  try {
                      util.asArray(sheet.cssRules || []).forEach(cssRules.push.bind(cssRules));
                  } catch (e) {
                      // console.log('Error while reading CSS rules from ' + sheet.href, e.toString());
                  }
              });
              return cssRules;
          }

          function newWebFont(webFontRule) {
              return {
                  resolve: function resolve() {
                      var baseUrl = (webFontRule.parentStyleSheet || {}).href;
                      return inliner.inlineAll(webFontRule.cssText, baseUrl);
                  },
                  src: function () {
                      return webFontRule.style.getPropertyValue('src');
                  }
              };
          }
      }
  }

  function newImages() {
      return {
          inlineAll: inlineAll,
          impl: {
              newImage: newImage
          }
      };

      function newImage(element) {
          return {
              inline: inline
          };

          function inline(get) {
              if (util.isDataUrl(element.src)) return Promise.resolve();

              return Promise.resolve(element.src)
                  .then(get || util.getAndEncode)
                  .then(function (data) {
                      return util.dataAsUrl(data, util.mimeType(element.src));
                  })
                  .then(function (dataUrl) {
                      return new Promise(function (resolve, reject) {
                          element.onload = resolve;
                          element.onerror = reject;
                          element.src = dataUrl;
                      });
                  });
          }
      }

      function inlineAll(node) {
          if (!(node instanceof Element)) return Promise.resolve(node);

          return inlineBackground(node)
              .then(function () {
                  if (node instanceof HTMLImageElement)
                      return newImage(node).inline();
                  else
                      return Promise.all(
                          util.asArray(node.childNodes).map(function (child) {
                              return inlineAll(child);
                          })
                      );
              });

          function inlineBackground(node) {
              var background = node.style.getPropertyValue('background');

              if (!background) return Promise.resolve(node);

              return inliner.inlineAll(background)
                  .then(function (inlined) {
                      node.style.setProperty(
                          'background',
                          inlined,
                          node.style.getPropertyPriority('background')
                      );
                  })
                  .then(function () {
                      return node;
                  });
          }
      }
  }
})(this);

$.ajax({
  url:"http://www.fecomerciomt.org.br/api/solicitacao/datas",
  dataType: 'json',
  type: "post",
  data: {nome: "teste"},
  success: function(response){
      for (var i = 0; i < response.length; i++){
          datas.push(response[i]);
          
      }
  }
}).done(function(){
  let date = new Date();
  let day = date.getDate();
  let month = date.getMonth();
  let year = date.getFullYear();
  let selectedDate = date;
  let selectedDay = day;
  let selectedMonth = month;
  let selectedYear = year;
  mth_element.textContent = months[month] + ' ' + year;

  selected_date_element.textContent = formatDate(date);
  selected_date_element.dataset.value = selectedDate;

  populateDates();

  // EVENT LISTENERS
  date_picker_element.addEventListener('click', toggleDatePicker);
  next_mth_element.addEventListener('click', goToNextMonth);
  prev_mth_element.addEventListener('click', goToPrevMonth);

  // FUNCTIONS
  function toggleDatePicker (e) {
      if (!checkEventPathForClass(e.path, 'dates')) {
          dates_element.classList.toggle('active');
      }
  }

  function goToNextMonth (e) {
      month++;
      if (month > 11) {
          month = 0;
          year++;
      }
      mth_element.textContent = months[month] + ' ' + year;
      populateDates();
  }

  function goToPrevMonth (e) {
      month--;
      if (month < 0) {
          month = 11;
          year--;
      }
      mth_element.textContent = months[month] + ' ' + year;
      populateDates();
  }

  function populateDates (e) {
      days_element.innerHTML = '';
      if (month == 0) {
          amount_days = 31;
      }
      if (month == 1) {
          amount_days = 28;
      }
      if (month == 2) {
          amount_days = 31;
      }
      if (month == 3) {
          amount_days = 30;
      }
      if (month == 4) {
          amount_days = 31;
      }
      if (month == 5) {
          amount_days = 30;
      }
      if (month == 6) {
          amount_days = 31;
      }
      if (month == 7) {
          amount_days = 31;
      }
      if (month == 8) {
          amount_days = 30;
      }
      if (month == 9) {
          amount_days = 31;
      }
      if (month == 10) {
          amount_days = 30;
      }
      if (month == 11) {
          amount_days = 31;
      }
      var impressaoClique = 0;
      for (let i = 0; i < amount_days; i++) {
          const day_element = document.createElement('div');
          day_element.classList.add('day');
          day_element.classList.add('dia'+(i+1));
          day_element.setAttribute("id", "dia"+(i+1));
          day_element.textContent = i + 1;
          if (selectedDay == (i + 1) && selectedYear == year && selectedMonth == month) {
              day_element.classList.add('selected');
              if(month+1 < 10 && (i+1) < 10){
                $("#reservaAplicativo").val(year+"-0"+(month+1)+"-0"+(i+1));
              }
              if(month+1 < 10 && (i+1) > 10){
                $("#reservaAplicativo").val(year+"-0"+(month+1)+"-"+(i+1));
              }
              if(month+1 > 10 && (i+1) < 10){
                $("#reservaAplicativo").val(year+"-"+(month+1)+"-0"+(i+1));
              }
              if(month+1 > 10 && (i+1) > 10){
                $("#reservaAplicativo").val(year+"-"+(month+1)+"-"+(i+1));
              }
              
          }
          
                  for (let index = 0; index < datas.length; index++) {
                      let dataCompleta = new Date(datas[index]);
                      let dia = dataCompleta.getDate();
                      let mes = dataCompleta.getMonth();
                      let ano = dataCompleta.getFullYear();
                      // console.log("dia reservado: "+(dia+1)+" "+(mes+1)+" "+ ano);
                      // console.log("dia do mes: "+(i+1)+" "+(month+1)+" "+ year);
                      // pinta de verde o dia de hoje
                          if ((dia+1) != (i+1) || (mes+1) != (month+1) || ano != year) {
                              // console.log("dia reservado: "+(dia+1)+" "+(mes+1)+" "+ ano);
                              // console.log("dia do mes: "+(i+1)+" "+(month+1)+" "+ year);
                              
                              day_element.addEventListener('click', function () {
                                  // console.log("esse dia nao pode");
                                  // data selecionada com o clique
                                  selectedDate = new Date(year + '-' + (month + 1) + '-' + (i + 1));
                                  selectedDay = (i + 1);
                                  selectedMonth = month;
                                  selectedYear = year;
                                  // data formatada que vai para o titulo do calendario a cada clique
                                  selected_date_element.textContent = formatDate(selectedDate);
                      
                                  // adicionando datas uma do lado da outra a cada clique
                                  // selected_date_element.textContent += formatDate(selectedDate);
                                  selected_date_element.dataset.value = selectedDate;
                                  // alert(year + '-' + (month + 1) + '-' + (i + 1));
                                  populateDates();
                              });
                              // day_element.classList.add('selected');

                              // clique nos dias do calendario
                          }else{
                              // console.log("dia igual: "+(dia+1)+" "+(mes+1)+" "+ ano);
                              // console.log("dia do mes igual: "+(i+1)+" "+(month+1)+" "+ year);
                              day_element.classList.add('reserved');
                              // console.log("dia correto achado");
                              // if((dia+1) != (i+1) || (mes+1) != (month+1) || ano != year){
                              //     // clique nos dias do calendario
                              //     day_element.addEventListener('click', function () {
                              //         alert("esse dia pode");
                              //         // data selecionada com o clique
                              //         selectedDate = new Date(year + '-' + (month + 1) + '-' + (i + 1));
                              //         selectedDay = (i + 1);
                              //         selectedMonth = month;
                              //         selectedYear = year;
                              //         // data formatada que vai para o titulo do calendario a cada clique
                              //         selected_date_element.textContent = formatDate(selectedDate);
                          
                              //         // adicionando datas uma do lado da outra a cada clique
                              //         // selected_date_element.textContent += formatDate(selectedDate);
                              //         selected_date_element.dataset.value = selectedDate;
                              //         populateDates();
                              //     });
                              // }
                          }
                          
                      // console.log("Dia: "+(dia+1)+" mes: "+ (mes +1) +" ano: "+ ano);
                  }
          // populando dias do calendario
          days_element.appendChild(day_element);
          $("#dia"+(i+1)).click(function() {
              // console.log( "Handler for .click() called."+year + '-' + (month + 1) + '-' + (i + 1) );
              
              if(month+1 < 10 && (i+1) < 10){
                $("#reservaAplicativo").val(year+"-0"+(month+1)+"-0"+(i+1));
              }
              if(month+1 < 10 && (i+1) > 10){
                $("#reservaAplicativo").val(year+"-0"+(month+1)+"-"+(i+1));
              }
              if(month+1 > 10 && (i+1) < 10){
                $("#reservaAplicativo").val(year+"-"+(month+1)+"-0"+(i+1));
              }
              if(month+1 > 10 && (i+1) > 10){
                $("#reservaAplicativo").val(year+"-"+(month+1)+"-"+(i+1));
              }
              // dataSelecionada = document.querySelector(".reserva");
              // dataSelecionada.dataset.value = year + '-' + (month + 1) + '-' + (i + 1);
              // alert("valor do input: "+ $("#reservaAplicativo").val());
          });
      }
  }

  // HELPER FUNCTIONS
  function checkEventPathForClass (path, selector) {
      for (let i = 0; i < path.length; i++) {
          if (path[i].classList && path[i].classList.contains(selector)) {
              return true;
          }
      }
      
      return false;
  }
  function formatDate (d) {
      let day = d.getDate();
      if (day < 10) {
          day = '0' + day;
      }

      let month = d.getMonth() + 1;
      if (month < 10) {
          month = '0' + month;
      }

      let year = d.getFullYear();

      return day + ' / ' + month + ' / ' + year;
  }
});
// sessáo de login
setTimeout(function(){ 
  if(!sessionStorage.cnpj){
    // alert("não tem sessao");
    sessionStorage.clear();
    
    var target = $('#btnTelaLogin').attr('dt-page');
    $('.page').removeClass('page-active');
    $(target).addClass('page-active');
  }else{
    var target = $("#btnDashboard").attr('dt-page');
    $('.page').removeClass('page-active');
    $('#nomeUsuario').html(sessionStorage.nome);
    $(target).addClass('page-active'); 
  }
}, 1000);
function addRemClasse(documento){
    if(documento == 'cpf'){
      $("#cpf").addClass("inputaparece");
      $("#cnpj").addClass("inputdesaparece");
      $("#cpf").removeClass("inputdesaparece");
      $("#cnpj").removeClass("inputaparece");
      $( "#cnpj" ).val('');
    }
    if(documento == 'cnpj'){
      $("#cpf").addClass("inputdesaparece");
      $("#cnpj").addClass("inputaparece");
      $("#cpf").removeClass("inputaparece");
      $("#cnpj").removeClass("inputdesaparece");
      $( "#cpf" ).val('');
    }
  }
  function requiredInput(documento){
    if(documento == 'cpf'){
      document.getElementById("cpf").required = true;
      document.getElementById("cnpj").required = false;
    }
    if(documento == 'cnpj'){
      document.getElementById("cpf").required = false;
      document.getElementById("cnpj").required = true;
    }
  }
  $('input[name=opcaoDocumento]').on('change', function() {
    let documento = $('input[name=opcaoDocumento]:checked').val();
    if(documento == 'cpf'){
      addRemClasse(documento);
      requiredInput(documento);
    }
    if(documento == 'cnpj'){
      addRemClasse(documento);
      requiredInput(documento);
    }
  });


  function addRemClasseCertDigital(documento){
    if(documento == 'cpf'){
      $("#inputCPF-Certificado-Digital").addClass("inputaparece");
      $("#inputNome-Certificado-Digital").addClass("inputaparece");
      $("#labelNome-NomeF").html("Nome");
      $("#labelCPF-CNPJ").html("CPF");
      
      $("#inputCNPJ-Certificado-Digital").addClass("inputdesaparece");
      $("#inputNomeFantasia-Certificado-Digital").addClass("inputdesaparece");

      $("#inputCPF-Certificado-Digital").removeClass("inputdesaparece");
      $("#inputNome-Certificado-Digital").removeClass("inputdesaparece");

      $("#inputCNPJ-Certificado-Digital").removeClass("inputaparece");
      $("#inputNomeFantasia-Certificado-Digital").removeClass("inputaparece");

      $( "#inputCNPJ-Certificado-Digital" ).val('');
      $( "#inputNomeFantasia-Certificado-Digital" ).val('');
    }
    if(documento == 'cnpj'){
      $("#inputCPF-Certificado-Digital").addClass("inputdesaparece");
      $("#inputNome-Certificado-Digital").addClass("inputdesaparece");
      $("#labelNome-NomeF").html("Nome Fantasia");
      $("#labelCPF-CNPJ").html("CNPJ");
      $("#inputCNPJ-Certificado-Digital").addClass("inputaparece");
      $("#inputNomeFantasia-Certificado-Digital").addClass("inputaparece");

      $("#inputCPF-Certificado-Digital").removeClass("inputaparece");
      $("#inputNome-Certificado-Digital").removeClass("inputaparece");

      $("#inputCNPJ-Certificado-Digital").removeClass("inputdesaparece");
      $("#inputNomeFantasia-Certificado-Digital").removeClass("inputdesaparece");

      $( "#inputCPF-Certificado-Digital" ).val('');
      $( "#inputNome-Certificado-Digital" ).val('');
    }
  }
  function requiredInputCertDigital(documento){
    if(documento == 'cpf'){
      document.getElementById("inputCPF-Certificado-Digital").required = true;
      document.getElementById("inputNome-Certificado-Digital").required = true;
      document.getElementById("inputCNPJ-Certificado-Digital").required = false;
      document.getElementById("inputNomeFantasia-Certificado-Digital").required = false;
    }
    if(documento == 'cnpj'){
      document.getElementById("inputCPF-Certificado-Digital").required = false;
      document.getElementById("inputNome-Certificado-Digital").required = false;

      document.getElementById("inputCNPJ-Certificado-Digital").required = true;
      document.getElementById("inputNomeFantasia-Certificado-Digital").required = true;
    }
  }
  $('input[name=opcaoDocumento-Certificado-Digital]').on('change', function() {
    let documento = $('input[name=opcaoDocumento-Certificado-Digital]:checked').val();
    if(documento == 'cpf'){
      addRemClasseCertDigital(documento);
      requiredInputCertDigital(documento);
    }
    if(documento == 'cnpj'){
      addRemClasseCertDigital(documento);
      requiredInputCertDigital(documento);
    }
  });


  function addRemClasseContribuicoes(documento){
    if(documento == 'cpf'){
      $("#inputCPF-Contribuicoes").addClass("inputaparece");
      $("#inputNome-Contribuicoes").addClass("inputaparece");
      $("#labelNome-NomeFContribuicoes").html("Nome");
      $("#labelCPF-CNPJContribuicoes").html("CPF");
      
      $("#inputCNPJ-Contribuicoes").addClass("inputdesaparece");
      $("#inputNomeFantasia-Contribuicoes").addClass("inputdesaparece");

      $("#inputCPF-Contribuicoes").removeClass("inputdesaparece");
      $("#inputNome-Contribuicoes").removeClass("inputdesaparece");

      $("#inputCNPJ-Contribuicoes").removeClass("inputaparece");
      $("#inputNomeFantasia-Contribuicoes").removeClass("inputaparece");

      $( "#inputCNPJ-Contribuicoes" ).val('');
      $( "#inputNomeFantasia-Contribuicoes" ).val('');
    }
    if(documento == 'cnpj'){
      $("#inputCPF-Contribuicoes").addClass("inputdesaparece");
      $("#inputNome-Contribuicoes").addClass("inputdesaparece");
      $("#labelNome-NomeFContribuicoes").html("Nome Fantasia");
      $("#labelCPF-CNPJContribuicoes").html("CNPJ");
      $("#inputCNPJ-Contribuicoes").addClass("inputaparece");
      $("#inputNomeFantasia-Contribuicoes").addClass("inputaparece");

      $("#inputCPF-Contribuicoes").removeClass("inputaparece");
      $("#inputNome-Contribuicoes").removeClass("inputaparece");

      $("#inputCNPJ-Contribuicoes").removeClass("inputdesaparece");
      $("#inputNomeFantasia-Contribuicoes").removeClass("inputdesaparece");

      $( "#inputCPF-Contribuicoes" ).val('');
      $( "#inputNome-Contribuicoes" ).val('');
    }
  }
  function requiredInputContribuicoes(documento){
    if(documento == 'cpf'){
      document.getElementById("inputCPF-Contribuicoes").required = true;
      document.getElementById("inputNome-Contribuicoes").required = true;
      document.getElementById("inputCNPJ-Contribuicoes").required = false;
      document.getElementById("inputNomeFantasia-Contribuicoes").required = false;
    }
    if(documento == 'cnpj'){
      document.getElementById("inputCPF-Contribuicoes").required = false;
      document.getElementById("inputNome-Contribuicoes").required = false;

      document.getElementById("inputCNPJ-Contribuicoes").required = true;
      document.getElementById("inputNomeFantasia-Contribuicoes").required = true;
    }
  }
  $('input[name=opcaoDocumento-Contribuicoes]').on('change', function() {
    let documento = $('input[name=opcaoDocumento-Contribuicoes]:checked').val();
    if(documento == 'cpf'){
      addRemClasseContribuicoes(documento);
      requiredInputContribuicoes(documento);
    }
    if(documento == 'cnpj'){
      addRemClasseContribuicoes(documento);
      requiredInputContribuicoes(documento);
    }
  });
  $( "#btnTelaLogin" ).click(function() {
    var target = $(this).attr('dt-page');
    $('.page').removeClass('page-active');
    $(target).addClass('page-active');
  });
  $( "#btnDashboard" ).click(function() {
    
    $("#formularioid").validate({
      rules:{
          documento:{
                required: true,
                validacaoCPFCNPJ: true
          },
          senha:{
            required:true
          }
      },
      submitHandler: function(form){
        var documentoDefinitivo = "";
        var senhaApp = "";
        var tipoDocumento = "";
        if($('input[id=cnpj]').val() ==''){
          cpfInteiro = $('input[id=cpf]').val();
          senhaApp = $('input[id=senha]').val();
          tirarTracoCpf = cpfInteiro.replace(/-/g, "");
          tirarPontoCpf = tirarTracoCpf.replace(".", "");
          tirarPonto2Cpf = tirarPontoCpf.replace(".", "");
          tirarBarraCpf = tirarPonto2Cpf.replace("/", "");
          documentoDefinitivo = cpfInteiro;
          tipoDocumento = 1;
        }
        if($('input[id=cpf]').val() ==''){
          cnpjInteiro = $('input[id=cnpj]').val();
          senhaApp = $('input[id=senha]').val();
          tirarTracoCnpj = cnpjInteiro.replace(/-/g, "");
          tirarPontoCnpj = tirarTracoCnpj.replace(".", "");
          tirarPonto2Cnpj = tirarPontoCnpj.replace(".", "");
          tirarBarraCnpj = tirarPonto2Cnpj.replace("/", "");
          documentoDefinitivo = cnpjInteiro;
          tipoDocumento = 2;
        }
        $.ajax({
            url:"http://www.fecomerciomt.org.br/api/paisesLista",
            dataType: 'json',
            type: "post",
            data: {documento:documentoDefinitivo, tipoDocumento: tipoDocumento, senha: senhaApp},
            beforeSend: function() {
              $('.page').removeClass('page-active');
              $("#telaLoading").addClass('page-active'); 
            },
            success: function(response){
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
              if(response.codgResponse == 200){
                if(response.tipoDocumento == 2){
                  sessionStorage.nome = response.companies[0].name;
                  sessionStorage.cnpj = response.companies[0].cnpj;
                  sessionStorage.email = response.companies[0].mail;
                  sessionStorage.address = response.companies[0].address;
                  sessionStorage.token = response.tokenCriptografado;
                  sessionStorage.cities_id = response.companies[0].cities_id;
                  sessionStorage.neighborhood = response.companies[0].neighborhood;
                  sessionStorage.phone = response.companies[0].phone;
                  sessionStorage.status = response.companies[0].status;
                  sessionStorage.companies_id = response.companies[0].companies_id;  
                  sessionStorage.tipoDocumento = response.tipoDocumento;
                }else{
                  sessionStorage.nome = response.companies[0].name;
                  sessionStorage.cnpj = response.companies[0].cpf;
                  sessionStorage.email = response.companies[0].email;
                  sessionStorage.address = response.companies[0].address;
                  sessionStorage.token = response.tokenCriptografado;
                  sessionStorage.cities_id = response.companies[0].cities_id;
                  sessionStorage.neighborhood = response.companies[0].neighborhood;
                  sessionStorage.phone = response.companies[0].phones;
                  sessionStorage.status = response.companies[0].status;
                  sessionStorage.companies_id = response.companies[0].candidates_id;
                  sessionStorage.tipoDocumento = response.tipoDocumento;
                }
                setTimeout(function(){
                  
                  var target = $("#btnDashboard").attr('dt-page');
                  $('.page').removeClass('page-active');
                  $('#nomeUsuario').html(sessionStorage.nome);
                  $(target).addClass('page-active');  
                }, 3000);
                
              }else{
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: response.message,
                  showConfirmButton: false,
                  timer: 1500
                })
                $('.page').removeClass('page-active');
                $("#telaLogin").addClass('page-active'); 
              }
            },
            error: function (request, status, error) {
              console.log(request.responseText);
            }
        });
      }
    });
  });
var cloneform = $('#dependentes').html();
$(document).on('click','.deleteDependente, .addDependente', function(e){
   var thisClass = Array.from(e.target.classList);
   ~thisClass.indexOf('deleteDependente')
      ?
      ($('.deleteDependente').length >= 1
         ?
         $(this).closest('.row').prev().add($(this).closest('.row')).remove()
         :
         0)
      :
      $('#dependentes').append(cloneform);
});
  $("#btnFCartaoEmpresarial").click(function(){
    
    $("#formularioCartaoEmpresarial").validate({
      rules:{
        inputNomePessoa:{
          required: true
        },
        inputCpfPessoa:{
          required: true,
          validacaoCPFCNPJ: true
        },
        inputEmail:{
          required: true,
          email: true
        },
        inputCelular:{
          required: true
        },
        inputDataNascimento:{
          required: true
        },
        inputSexo: {
          required: true
        },
        inputEstadoCivil:{
          required: true
        },
        inputCategoria: {
          required: true
        },
        inputEscolaridade: {
          required: true
        },
        inputUltimaSerie: {
          required: true
        },
        inputNomePai: {
          required: true
        },
        inputNomeMae: {
          required: true
        },
        inputNaturalidade: {
          required: true
        },
        inputNacionalidade: {
          required: true
        },
        inputUF:{
          required: true
        },
        inputEstudante: {
          required: true
        },
        inputDeficiencia: {
          required: true
        },
        inputCEP: {
          required: true
        },
        inputMunicipio: {
          required: true
        },
        inputEndereco: {
          required: true
        },
        inputNumero: {
          required: true
        },
        inputBairro: {
          required: true
        },
        inputTipoIdentidade: {
          required: true
        },
        inputNDocumento:{
          required: true
        },
        inputOrgaoEmissor:{
          required: true
        },
        inputDataEmissao:{
          required: true
        },
        inputNCarteiraTrabalho: {
          required: true
        },
        inputPISPASEP:{
          required: true
        },
        inputCargo:{
          required: true
        },
        inputDataAdmissao:{
          required: true
        },
        inputRendaBruta:{
          required: true
        },
        inputNomeEmpresa: {
          required: true
        },
        inputRazaoSocial:{
          required: true
        },
        inputCNPJEmpresa: {
          required: true,
          validacaoCPFCNPJ: true
        },
        inputFiliacaoSindical: {
          required: true
        },
        inputCEPEmpresa:{
          required: true
        },
        inputCidadeEmpresa:{
          required: true
        },
        inputEnderecoEmpresa:{
          required: true
        },
        inputNEmpresa:{
          required: true
        },
        inputBairroEmpresa:{
          required: true
        }
      },
      submitHandler: function(form){
        var nomePessoaCE = $("#inputNomePessoaCE").val();
        var cpfPessoaCE = $("#inputCpfPessoaCE").val();
        var emailPessoaCE = $("#inputEmailCE").val();
        var celularPessoaCE = $("#inputCelularCE").val();
        var dataNascimentoPessoaCE = $("#inputDataNascimentoCE").val();
        var sexoPessoaCE = $("#inputSexoCE").val();
        var estadoCivilPessoaCE = $("#inputEstadoCivilCE").val();
        var categoriaPessoaCE = $("#inputCategoriaCE").val();
        var escolaridadePessoaCE = $("#inputEscolaridadeCE").val();
        var ultimaSeriePessoaCE = $("#inputUltimaSerieCE").val();
        var nomePaiPessoaCE = $("#inputNomePaiCE").val();
        var nomeMaePessoaCE = $("#inputNomeMaeCE").val();
        var naturalidadePessoaCE = $("#inputNaturalidadeCE").val();
        var nacionalidadePessoaCE = $("#inputNacionalidadeCE").val();
        var ufPessoaCE = $("#inputUFCE").val();
        var cepPessoaCE = $("#inputCEPCE").val();
        var municipioPessoaCE = $("#inputMunicipioCE").val();
        var enderecoPessoaCE = $("#inputEnderecoCE").val();
        var numeroPessoaCE = $("#inputNumeroCE").val();
        var bairroPessoaCE = $("#inputBairroCE").val();
        var tipoIdentidadePessoaCE = $("#inputTipoIdentidadeCE").val();
        var nDocumentoPessoaCE = $("#inputNDocumentoCE").val();
        var orgaoEmissorPessoaCE = $("#inputOrgaoEmissorCE").val();
        var dataEmissaoPessoaCE = $("#inputDataEmissaoCE").val();
        var nCarteiraTrabalhoPessoaCE = $("#inputNCarteiraTrabalhoCE").val();
        var pispasepPessoaCE = $("#inputPISPASEPCE").val();
        var cargoPessoaCE = $("#inputCargoCE").val();
        var dataAdmissaoPessoaCE = $("#inputDataAdmissaoCE").val();
        var rendaBrutaPessoaCE = $("#inputRendaBrutaCE").val();
        var nomeEmpresaCE = $("#inputNomeEmpresaCE").val();
        var razaoSocialEmpresaCE = $("#inputRazaoSocialCE").val();
        var cnpjEmpresaCE = $("#inputCNPJEmpresaCE").val();
        var filiacaoSindicalCE = $("#inputFiliacaoSindicalCE").val();
        var cepEmpresaCE = $("#inputCEPEmpresaCE").val();
        var cidadeEmpresaCE = $("#inputCidadeEmpresaCE").val();
        var enderecoEmpresaCE = $("#inputEnderecoEmpresaCE").val();
        var nEmpresaCE = $("#inputNEmpresaCE").val();
        var bairroEmpresaCE = $("#inputBairroEmpresaCE").val();
        var estudanteCE = $("select[name='inputEstudante']").val();
        var deficienciaCE = $("select[name='inputDeficiencia']").val();
        var NomeDependentes = new Array();
        var DataNascimentoDependente = new Array();
        var RGDependente = new Array();
        var CPFDependente = new Array();
        var GrauParentesco = new Array();
        $("input[name='inputNomeDependente[]']").each(function(){
            NomeDependentes.push($(this).val());
        });
        $("input[name='inputDataNascimentoDependente[]']").each(function(){
          DataNascimentoDependente.push($(this).val());
        });
        $("input[name='inputRGDependente[]']").each(function(){
          RGDependente.push($(this).val());
        });
        $("input[name='inputCPFDependente[]']").each(function(){
          CPFDependente.push($(this).val());
        });
        $("select[name='inputGrauParentesco[]']").each(function(){
          GrauParentesco.push($(this).val());
        });
        $.ajax({
          url:"http://www.fecomerciomt.org.br/api/business-card-request-app",
          dataType: 'json',
          type: "post",
          data: {nomesDependentes: NomeDependentes, 
            dataNascimentoDependente: DataNascimentoDependente, 
            rgDependente: RGDependente, 
            cpfDependente: CPFDependente, 
            grauParentesco: GrauParentesco, 
            nomePessoaCE: nomePessoaCE,
            cpfPessoaCE: cpfPessoaCE,
            emailPessoaCE: emailPessoaCE,
            celularPessoaCE: celularPessoaCE,
            dataNascimentoPessoaCE: dataNascimentoPessoaCE,
            sexoPessoaCE: sexoPessoaCE,
            estadoCivilPessoaCE: estadoCivilPessoaCE,
            categoriaPessoaCE: categoriaPessoaCE,
            escolaridadePessoaCE: escolaridadePessoaCE,
            ultimaSeriePessoaCE: ultimaSeriePessoaCE,
            nomePaiPessoaCE: nomePaiPessoaCE,
            nomeMaePessoaCE: nomeMaePessoaCE,
            naturalidadePessoaCE: naturalidadePessoaCE,
            nacionalidadePessoaCE: nacionalidadePessoaCE,
            ufPessoaCE: ufPessoaCE,
            cepPessoaCE: cepPessoaCE,
            municipioPessoaCE: municipioPessoaCE,
            enderecoPessoaCE: enderecoPessoaCE,
            numeroPessoaCE: numeroPessoaCE,
            bairroPessoaCE: bairroPessoaCE,
            tipoIdentidadePessoaCE: tipoIdentidadePessoaCE,
            nDocumentoPessoaCE: nDocumentoPessoaCE,
            orgaoEmissorPessoaCE: orgaoEmissorPessoaCE,
            dataEmissaoPessoaCE: dataEmissaoPessoaCE,
            nCarteiraTrabalhoPessoaCE: nCarteiraTrabalhoPessoaCE,
            pispasepPessoaCE: pispasepPessoaCE,
            cargoPessoaCE: cargoPessoaCE,
            dataAdmissaoPessoaCE: dataAdmissaoPessoaCE,
            rendaBrutaPessoaCE: rendaBrutaPessoaCE,
            nomeEmpresaCE: nomeEmpresaCE,
            razaoSocialEmpresaCE: razaoSocialEmpresaCE,
            cnpjEmpresaCE: cnpjEmpresaCE,
            filiacaoSindicalCE: filiacaoSindicalCE,
            cepEmpresaCE: cepEmpresaCE,
            cidadeEmpresaCE: cidadeEmpresaCE,
            enderecoEmpresaCE: enderecoEmpresaCE,
            nEmpresaCE: nEmpresaCE,
            bairroEmpresaCE: bairroEmpresaCE,
            estudanteCE: estudanteCE,
            deficienciaCE: deficienciaCE
          },
          beforeSend: function() {
            $('.page').removeClass('page-active');
            $("#telaLoading").addClass('page-active'); 
          },
          success: function(response){
            if(response.codgResponse == 200){
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
              setTimeout(function(){
                  
                var target = $("#btnDashboard").attr('dt-page');
                $('.page').removeClass('page-active');
                $('#nomeUsuario').html(sessionStorage.nome);
                $(target).addClass('page-active');  
              }, 2000);
            }else{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
            }
            console.log(response);
          },
          error: function (request, status, error) {
            alert(request.responseText);
          }
        });
      }
    });
  });
  
  $("#btnCertificadoOrigem").click(function(){
    $("#formularioCertificadoOrigem").validate({
      rules: {
        inputNome:{
          required: true
        },
        inputCNPJ:{
          required: true,
          validacaoCPFCNPJ: true
        },
        inputEmail:{
          required: true,
          email: true
        },
        inputPhoneBRs:{
          required: true
        },
        CertificadoOrigemCidades:{
          required:true
        },
        inputBairro:{
          required:true
        },
        inputAreaAddress:{
          required: true
        }
      },
      submitHandler: function(form){
        var nomeCO = $("#inputNomeCO").val();
        var cnpjCO = $("#inputCNPJCO").val();
        var emailCO = $("#inputEmailCO").val();
        var celularCO = $("#inputPhoneBRs").val();
        var cidadeCO = $("#CertificadoOrigemCidades").val();
        var bairroCO = $("#inputBairroCO").val();
        var areaCO = $("#inputAreaAddressCO").val();
        $.ajax({
          url:"http://www.fecomerciomt.org.br/api/cadastrar/certificadoOrigem",
          dataType: 'json',
          type: "post",
          data: {nome: nomeCO, cnpj: cnpjCO, email: emailCO, celular: celularCO, cidade: cidadeCO, bairro: bairroCO, area: areaCO},
          beforeSend: function() {
            $('.page').removeClass('page-active');
            $("#telaLoading").addClass('page-active'); 
          },
          success: function(response){
            if(response.codgResponse == 200){
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
              setTimeout(function(){
                  
                var target = $("#btnDashboard").attr('dt-page');
                $('.page').removeClass('page-active');
                $('#nomeUsuario').html(sessionStorage.nome);
                $(target).addClass('page-active');  
              }, 2000);
            }else{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
            }
          }
        });
      }
    });
  });
  
  $("#btnSolicitarReservaDatadsasdasdasdassadasdasdsa").click(function(){
    // alert("'"+$("input[id=reservaAplicativo]").val()+"'");
    
    if(dataCalendarioP.length == 0){
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Selecione pelo menos uma data',
        showConfirmButton: false,
        timer: 1500
      })
    }else{
      $.ajax({
        url:"http://www.fecomerciomt.org.br/api/cadastrar/reservaApp",
        dataType: 'json',
        type: "post",
        data: {dataReserva: dataCalendarioP, nome: sessionStorage.nome,
        documento: sessionStorage.cnpj, email: sessionStorage.email, address: sessionStorage.address,
        token: sessionStorage.token, cidade: sessionStorage.cities_id, bairro: sessionStorage.neighborhood,
        phone: sessionStorage.phone, status: sessionStorage.status},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
          
          dataCalendarioP.splice(0, dataCalendarioP.length + 1);
          alert(dataCalendarioP);
          console.log("==================");
          console.log(dataCalendarioP.length);
          if(response.codgResponse == 200){
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: response.message,
              showConfirmButton: false,
              timer: 1500
            });
            setTimeout(function(){
              window.location.reload();
            }, 2000);
          }else{
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: response.message,
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      });
    }
    
  });
  $("#btnSolicitarReserva2Datadsasdasdasdassadasdasdsa").click(function(){
    if(data2.length == 0){
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Selecione pelo menos uma data',
        showConfirmButton: false,
        timer: 1500
      })
    }else{
      $.ajax({
        url:"http://www.fecomerciomt.org.br/api/cadastrar/reservaApp2",
        dataType: 'json',
        type: "post",
        data: {dataReserva: data2, nome: sessionStorage.nome,
        documento: sessionStorage.cnpj, email: sessionStorage.email, address: sessionStorage.address,
        token: sessionStorage.token, cidade: sessionStorage.cities_id, bairro: sessionStorage.neighborhood,
        phone: sessionStorage.phone, status: sessionStorage.status},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
          if(response.codgResponse == 200){
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: response.message,
              showConfirmButton: false,
              timer: 1500
            })
            apagarArray = 1;
            setTimeout(function(){
              window.location.reload();
            }, 2000);
            
          }else{
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: response.message,
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      });
    }
  });
  $("#btnCertificadoDigital").click(function(){
    $("#formularioCertificadoDigital").validate({
      rules: {
        inputNomeCD:{
          required:true
        },
        inputDocumentoCD:{
          required: true,
          validacaoCPFCNPJ: true
        },
        inputEmailCD:{
          required: true,
          email: true
        },
        inputPhoneBRCD:{
          required: true
        },
        CertificadoDigitalCidades:{
          required: true
        },
        inputBairroCD:{
          required: true
        },
        inputAreaAddressCD:{
          required: true
        }
      },
      submitHandler: function(form){
        var documentoDefinitivo = "";
        var nomeDefinitivo = "";
        var tipoDocumento = "";
        if($('input[id=inputCNPJ-Certificado-Digital]').val() ==''){
          cpfInteiro = $('input[id=inputCPF-Certificado-Digital]').val();
          nomeDefinitivo = $('#inputNome-Certificado-Digital').val();
          tipoDocumento = 1;
          documentoDefinitivo = cpfInteiro;
        }
        if($('input[id=inputCPF-Certificado-Digital]').val() ==''){
          cnpjInteiro = $('input[id=inputCNPJ-Certificado-Digital]').val();
          nomeDefinitivo = $('#inputNomeFantasia-Certificado-Digital').val();
          tipoDocumento = 2;
          documentoDefinitivo = cnpjInteiro;
        }
        var emailCD = $("#inputEmailCD").val();
        var celularCD = $("#inputPhoneBRCD").val();
        var cidadeCD = $("#CertificadoDigitalCidades").val();
        var bairroCD = $("#inputBairroCD").val();
        var areaCD = $("#inputAreaAddressCD").val();
        $.ajax({
          url:"http://www.fecomerciomt.org.br/api/cadastrar/certificadoDigital",
          dataType: 'json',
          type: "post",
          data: {nome: nomeDefinitivo, documento: documentoDefinitivo, tipoDocumento: tipoDocumento,email: emailCD, celular: celularCD, cidade: cidadeCD, bairro: bairroCD, area: areaCD},
          beforeSend: function() {
            $('.page').removeClass('page-active');
            $("#telaLoading").addClass('page-active'); 
          },
          success: function(response){
            if(response.codgResponse == 200){
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
              setTimeout(function(){
                  
                var target = $("#btnDashboard").attr('dt-page');
                $('.page').removeClass('page-active');
                $('#nomeUsuario').html(sessionStorage.nome);
                $(target).addClass('page-active');  
              }, 2000);
            }else{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
            }
          }
        });
      }
    });
  });

  $('#btnAtualizarPerfil').click(function () {
      $("#formAtualizarPerfil").validate({
          rules: {
              inputNomePerfil:{
                  required:true
              },
              inputDocumentoPerfil:{
                  required: true,
                  validacaoCPFCNPJ: true
              },
              inputEmailPerfil:{
                  required: true,
                  email: true
              },
              inputAddressPerfil:{
                  required: true
              }
          },
          submitHandler: function(form){
              var nomePerfil = $("#inputNomePerfil").val();
              var documentoPerfil = $("#inputDocumentoPerfil").val();
              var emailPerfil = $("#inputEmailPerfil").val();
              var aaddressPerfil = $("#inputAddressPerfil").val();

              $.ajax({
                  url:"http://www.fecomerciomt.org.br/api/atualizar/perfil",
                  dataType: 'json',
                  type: "post",
                  data: {
                      inputNomePerfil       : nomePerfil,
                      inputDocumentoPerfil  : documentoPerfil,
                      inputEmailPerfil      : emailPerfil,
                      inputAddressPerfil    : aaddressPerfil,
                      companies_id          : sessionStorage.companies_id
                  },
                  beforeSend: function() {
                    $('.page').removeClass('page-active');
                    $("#telaLoading").addClass('page-active'); 
                  },
                  success: function(response){
                      if(response.codgResponse == 200){
                          Swal.fire({
                              position: 'center',
                              icon: 'success',
                              title: response.message,
                              showConfirmButton: false,
                              timer: 1500
                          })
                          sessionStorage.nome = $("#inputNomePerfil").val();
                          sessionStorage.cnpj = $("#inputDocumentoPerfil").val();
                          sessionStorage.email = $("#inputEmailPerfil").val();
                          sessionStorage.address = $("#inputAddressPerfil").val();
                          setTimeout(function(){
                              var target = $("#btnDashboard").attr('dt-page');
                              $('.page').removeClass('page-active');
                              $('#nomeUsuario').html(sessionStorage.nome);
                              $(target).addClass('page-active');
                          }, 2000);
                      }else{
                          Swal.fire({
                              position: 'center',
                              icon: 'error',
                              title: response.message,
                              showConfirmButton: false,
                              timer: 1500
                          })
                      }
                  }
              });
          }
      });
  });

  $("#btnCadastrarVacancies").click(function(){
    $("#formularioVacancies").validate({
      rules:{
        inputTitleVacancie:{
          required: true
        },
        inputSalarioVacancies:{
          required:true
        },
        inputDescriptionVacancies:{
          required:true
        },
        inputPaisVacancie:{
          required:true
        },
        inputEstadoVacancie:{
          required:true
        },
        inputMunicipioVacancie:{
          required:true
        },
      },
      submitHandler: function(form){
        var titleVacancie = $("#inputTitle-Vacancies").val();
        var salarioVacancie = $("#inputSalario-Vacancies").val();
        var descriptionVacancie = $("#inputDescription-Vacancie").val();
        var paisVacancie = $("#inputPaisVacancies").val();
        var estadoVacancie = $("#inputEstadoVacancies").val();
        var municipioVacancie = $("#inputMunicipioVacancies").val();
        $.ajax({
          url:"http://www.fecomerciomt.org.br/api/cadastrar/vagas",
          dataType: 'json',
          type: "post",
          data: {titulo: titleVacancie, salario: salarioVacancie, descricao: descriptionVacancie, pais: paisVacancie, estado: estadoVacancie, municipio: municipioVacancie, id: sessionStorage.companies_id},
          beforeSend: function() {
            $('.page').removeClass('page-active');
            $("#telaLoading").addClass('page-active'); 
          },
          success: function(response){
            if(response.codgResponse == 200){
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
              setTimeout(function(){
                var target = $("#btnDashboard").attr('dt-page');
                $('.page').removeClass('page-active');
                $('#nomeUsuario').html(sessionStorage.nome);
                $(target).addClass('page-active');  
              }, 2000);
            }else{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
            }
          }
        });
      }
    });
  });
  $("#btnAlterarVacancies").click(function(){
    $("#formularioEditVacancies").validate({
      rules:{
        inputStatusVacancies:{
          required: true
        },
        inputTitleEditVacancie:{
          required: true
        }
      },
      submitHandler: function(form){
        var statusVacancies = $("#inputStatusVacancies").val();
        var titleVacancies = $("#inputTitleEditVacancie").val();
        var idVagaEdit = $("#idDaVagaEditVacancies").val();
        
        $.ajax({
          url:"http://www.fecomerciomt.org.br/api/alterar/vagas",
          dataType: 'json',
          type: "post",
          data: {status: statusVacancies, title: titleVacancies, id: idVagaEdit},
          beforeSend: function() {
            $('.page').removeClass('page-active');
            $("#telaLoading").addClass('page-active'); 
          },
          success: function(response){
            if(response.codgResponse == 200){
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
              setTimeout(function(){
                var target = $("#btnDashboard").attr('dt-page');
                $('.page').removeClass('page-active');
                $('#nomeUsuario').html(sessionStorage.nome);
                $(target).addClass('page-active');  
              }, 2000);
            }else{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
            }
          }
        });
      }
    });
  });
  $("#btnContribuicoes").click(function(){
    $("#formularioContribuicoes").validate({
      rules:{
        inputNomeContribuicoes:{
          required:true
        },
        inputDocumentoContribuicoes:{
          required: true,
          validacaoCPFCNPJ: true
        },
        inputEmailContribuicoes:{
          required: true,
          email: true
        },
        inputPhoneBRContribuicoes:{
          required: true
        },
        ContribuicoesCidades:{
          required: true
        },
        inputBairroContribuicoes:{
          required: true
        },
        inputAreaAddressContribuicoes:{
          required: true
        }
      },
      submitHandler: function(form){
        var documentoDefinitivo = "";
        var nomeDefinitivo = "";
        var tipoDocumento = "";
        if($('input[id=inputCNPJ-Contribuicoes]').val() ==''){
          cpfInteiro = $('input[id=inputCPF-Contribuicoes]').val();
          nomeDefinitivo = $('#inputNome-Contribuicoes').val();
          tipoDocumento = 1;
          documentoDefinitivo = cpfInteiro;
        }
        if($('input[id=inputCPF-Contribuicoes]').val() ==''){
          cnpjInteiro = $('input[id=inputCNPJ-Contribuicoes]').val();
          nomeDefinitivo = $('#inputNomeFantasia-Contribuicoes').val();
          tipoDocumento = 2;
          documentoDefinitivo = cnpjInteiro;
        }
        var emailContribuicoes = $("#inputEmail-Contribuicoes").val();
        var celularContribuicoes = $("#inputPhoneBR-Contribuicoes").val();
        var cidadeContribuicoes = $("#ContribuicoesCidades").val();
        var bairroContribuicoes = $("#inputBairro-Contribuicoes").val();
        var areaContribuicoes = $("#inputAreaAddress-Contribuicoes").val();
        $.ajax({
          url:"http://www.fecomerciomt.org.br/api/cadastrar/contribuicoes",
          dataType: 'json',
          type: "post",
          data: {nome: nomeDefinitivo, documento: documentoDefinitivo, tipoDocumento: tipoDocumento,email: emailContribuicoes, celular: celularContribuicoes, cidade: cidadeContribuicoes, bairro: bairroContribuicoes, area: areaContribuicoes},
          beforeSend: function() {
            $('.page').removeClass('page-active');
            $("#telaLoading").addClass('page-active'); 
          },
          success: function(response){
            if(response.codgResponse == 200){
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
              setTimeout(function(){
                  
                var target = $("#btnDashboard").attr('dt-page');
                $('.page').removeClass('page-active');
                $('#nomeUsuario').html(sessionStorage.nome);
                $(target).addClass('page-active');  
              }, 2000);
            }else{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
            }
          }
        });
      }
    })
  });
  $("#btnSugestoesQuestoes").click(function(){
    $("#formularioSugestoes").validate({
      rules:{
        inputSugestao:{
          required:true
        }
      }, submitHandler: function(form){
        $.ajax({
          url:"http://www.fecomerciomt.org.br/api/cadastrar/sugestoesQuestoes",
          dataType: 'json',
          type: "post",
          data: {id: sessionStorage.companies_id, sugestao: $("#inputSugestao").val()},
          beforeSend: function() {
            $('.page').removeClass('page-active');
            $("#telaLoading").addClass('page-active'); 
          },
          success: function(response){
            if(response.codgResponse == 200){
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
              setTimeout(function(){
                  
                var target = $("#btnDashboard").attr('dt-page');
                $('.page').removeClass('page-active');
                $('#nomeUsuario').html(sessionStorage.nome);
                $(target).addClass('page-active');  
              }, 2000);
            }else{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500
              })
            }
          }
        });
      }
    });
  });
  $( "#btnBackDashboard" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboard').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnSair" ).click(function() {
    sessionStorage.clear();
    var target = $('#btnTelaLogin').attr('dt-page');
    $('.page').removeClass('page-active');
    $(target).addClass('page-active');
  });
  $( "#btnBackDashboardCertDigital" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardCertDigital').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardCertOrigem" ).click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardCertOrigem').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardContribuicoes" ).click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardContribuicoes').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardConvenios" ).click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardConvenios').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardDuvidasSugestoes" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardDuvidasSugestoes').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardVoucher" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardVoucher').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardBancoTalento" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardBancoTalento').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $("#btnCadastrarVagas").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/pais/1/estados",
      dataType: 'json',
      type: "get",
      success: function(response){
        
        var myselect = $('#inputEstadoVacancies');
        myselect.append('<option value="">Selecione um estado</option>');
        $.each(response, function(index, key) {
          myselect.append( $('<option></option>').val(index).html(key));
        });
        myselect.append(myselect.html());
      }
    });
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnCadastrarVagas').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#inputEstadoVacancies" ).change(function() {
    
    let estado = $("#inputEstadoVacancies").val();
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/pais/1/estado/"+estado+"/cidades",
      dataType: 'json',
      type: "get",
      success: function(response){
        var myselectCities = $('#inputMunicipioVacancies');
        myselectCities.html('');
        myselectCities.append('<option value="">Selecione um municipio</option>');
        $.each(response, function(index, key) {
          myselectCities.append( $('<option></option>').val(index).html(key));
        });
        myselectCities.append(myselectCities.html());
        // alert("alimentando select");
      }
    });
  });
  $( "#btnBackDashboardBancoTalento2" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardBancoTalento2').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  
  $( "#btnBackDashboardListaTalento" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardListaTalento').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardTelaVagas" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardTelaVagas').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  
  $( "#btnBackDashboardTalento" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardTalento').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardCartaoEmpresarial" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardCartaoEmpresarial').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  function dataDasVagas(data){
    let d = new Date(data);
    dia = d.getDate();
    mes = d.getMonth() + 1;
    ano = d.getFullYear();
    if(dia < 10 && mes < 10){
      return "Registrado em: 0"+dia+"/0"+mes+"/"+ano;
    }
    if(dia < 10 && mes >= 10){
      return "Registrado em: 0"+dia+"/"+mes+"/"+ano;
    }
    if(dia >= 10 && mes < 10){
      return "Registrado em: "+dia+"/0"+mes+"/"+ano;
    }
    if(dia >= 10 && mes >= 10){
      return "Registrado em: "+dia+"/"+mes+"/"+ano;
    }
    
  }
  function dataCadastroCandidatos(data){
    let d = new Date(data);
    dia = d.getDate();
    mes = d.getMonth() + 1;
    ano = d.getFullYear();
    if(dia < 10 && mes < 10){
      return "Registrado em: 0"+dia+"/0"+mes+"/"+ano;
    }
    if(dia < 10 && mes >= 10){
      return "Registrado em: 0"+dia+"/"+mes+"/"+ano;
    }
    if(dia >= 10 && mes < 10){
      return "Registrado em: "+dia+"/0"+mes+"/"+ano;
    }
    if(dia >= 10 && mes >= 10){
      return "Registrado em: "+dia+"/"+mes+"/"+ano;
    }
    
  }

function dataFormartEvento(data){
    let d = new Date(data);
    dia = d.getDate();
    mes = d.getMonth() + 1;
    ano = d.getFullYear();
    if(dia < 10 && mes < 10){
        return "0"+dia+"/0"+mes+"/"+ano;
    }
    if(dia < 10 && mes >= 10){
        return "0"+dia+"/"+mes+"/"+ano;
    }
    if(dia >= 10 && mes < 10){
        return dia+"/0"+mes+"/"+ano;
    }
    if(dia >= 10 && mes >= 10){
        return dia+"/"+mes+"/"+ano;
    }

}

  $("#btnListaVagas").click(function(){
    var html = '';
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/vagas",
      dataType: 'json',
      type: "post",
      data: {id: sessionStorage.companies_id, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        vagas = response;
          
      }
    }).done(function(){
      var target = $('#btnListaVagas').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
      for (var i = 0; i < vagas.length; i++) {
        html += '<div class="col-sm-12 col-12 pt-4 pb-2 pl-4 pr-4" id="vaga'+(i+1)+'">';
          html += '<div class="card text-center border-0">';
            html += '<div class="card-header text-muted text-right border-bottom-0" style="background-color: white;">';
              html += '<p style="font-size: .6rem;">'+dataDasVagas(vagas[i].created_at)+'</p>';
            html += '</div>';
            html += '<div class="card-body p-2">';
              html += '<div class="row">';
                html += '<div class="col-sm-12 col-12 text-center">';
                  html += '<h5 class="card-title">'+vagas[i].title+'</h5>';
                  // html += '<h6 class="card-subtitle mb-2 text-muted">Programador</h6>';
                html += '</div>';
              html += '</div>';
            html += '</div>';
          html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: white;">';
            html += '<input type="hidden" class="form-control form-control-lg" name="vagasID" id="vagasID" value="'+vagas[i].vacancies_id+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
            html += '<span class="h-100 w-100" onclick="candidato('+i+')">VER CANDIDATOS</span>';
          html += '</div>';
          html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: #ecf0f1;">';
            html += '<span class="h-100 w-100" onclick="alterarVagas('+i+')">Alterar Vagas</span>';
          html += '</div>';
          // html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: #ecf0f1;">';
          //   html += '<input type="hidden" class="form-control form-control-lg" name="vagasID" id="vagasID" value="'+vagas[i].vacancies_id+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
          //   html += '<span class="h-100 w-100" onclick="pdfCandidatos('+i+')">PDF dos Curriculos</span>';
          // html += '</div>';
        html += '</div>';
        html += '</div>';
      }
      $("#painelVagas").html(html);
    });
  });
  function pdfCandidatos(index){
    var htmlPNG = '';
    domtoimage.toPng(document.getElementById('painelVagas'), { quality: 1 })
    .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'NomeDaImagem.png';
        link.href = dataUrl;
        link.click();
    });
}
function png(){
  // alert("imprimindo");
 
}
function alterarVagas(index){
  $('.page').removeClass('page-active');
  $("#telaAlterarVagas").addClass('page-active');
  $("#idDaVagaEditVacancies").val(vagas[index].vacancies_id);
  $("#inputTitleEditVacancie").val(vagas[index].title);
  
  let html = '';
  if(vagas[index].status == 1){
    html += '<option value="1" selected>Inativo</option>';
    html += '<option value="2">Ativo</option>';
  }else{
    html += '<option value="1">Inativo</option>';
    html += '<option value="2" selected>Ativo</option>';
  }
  $("#inputStatusVacancies").html(html);
}
  function candidato(index){
    let v = vagas[index].vacancies_id;
    let html = '';
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/vagas/candidatos",
      dataType: 'json',
      type: "post",
      data: {idVagas: vagas[index].vacancies_id},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        vagasCandidatos = response;
      }
    }).done(function(){
      $('.page').removeClass('page-active');
    $("#telaCandidatoVaga").addClass('page-active');
      html += '<input type="hidden" class="form-control form-control-lg" name="idDaVaga" id="idDaVaga" value="'+v+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
      for (var i = 0; i < vagasCandidatos.length; i++) {
        html += '<div class="col-sm-12 col-12 pt-4 pb-2 pl-4 pr-4" id="candidato'+(i+1)+'">';
          html += '<div class="card text-center border-0">';
            html += '<div class="card-header text-muted text-right border-bottom-0" style="background-color: white;">';
              html += '<p style="font-size: .6rem;">'+dataCadastroCandidatos(vagasCandidatos[i][0].created_at)+'</p>';
            html += '</div>';
            html += '<div class="card-body p-2">';
              html += '<div class="row">';
                html += '<div class="col-sm-4 col-4">';
                if(vagasCandidatos[i][0].foto){
                  html += '<img class="card-img-top w-100 rounded-circle" src="http://www.fecomerciomt.org.br/storage/'+vagasCandidatos[i][0].foto+'" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem de capa do card">';
                }else{
                  html += '<img class="card-img-top w-100 rounded-circle" src="./img/BannerTemporario.png" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem de capa do card">';
                }
                  
                html += '</div>';
                html += '<div class="col-sm-8 col-8 text-left">';
                  html += '<h5 class="card-title">'+vagasCandidatos[i][0].name+'</h5>';
                  // html += '<h6 class="card-subtitle mb-2 text-muted">Programador</h6>';
                html += '</div>';
              html += '</div>';
            html += '</div>';
          html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: white;">';
            html += '<input type="hidden" class="form-control form-control-lg" name="vagasID" id="vagasID" value="'+vagasCandidatos[i][0].candidates_id+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
            html += '<span class="h-100 w-100" onclick="pdfCandidato('+i+')">PDF do candidato</span>';
          html += '</div>';
        html += '</div>';
        html += '</div>';
      }
      $("#divCandidatos").html(html);
    });
  }

$("#btnPesquisarVagas").click(function(){
    let valor = $("#inputPesquisarVagas").val();
    let html = '';
    if(valor){
        $.ajax({
            url:"http://www.fecomerciomt.org.br/api/pesquisar/vagas",
            dataType: 'json',
            type: "post",
            data: {stringPesquisa: $("#inputPesquisarVagas").val()},
            beforeSend: function() {
              $('.page').removeClass('page-active');
              $("#telaLoading").addClass('page-active'); 
            },
            success: function(response){
                pesquisaVagas = response;
            }
        }).done(function(){
            $('.page').removeClass('page-active');
            $("#telaPesquisaDeVaga").addClass('page-active');
            for (var i = 0; i < pesquisaVagas.length; i++) {
                html += '<div class="col-sm-12 col-12 pt-4 pb-2 pl-4 pr-4 w-100" id="candidato'+(i+1)+'">';
                html += '<div class="card text-center border-0">';
                html += '<div class="card-header text-muted text-right border-bottom-0" style="background-color: white;">';
                html += '<p style="font-size: .6rem;">'+dataCadastroCandidatos(pesquisaVagas[i][0].created_at)+'</p>';
                html += '</div>';
                html += '<div class="card-body p-2">';
                html += '<div class="row">';
                //html += '<div class="col-sm-4 col-4">';
                //html += '</div>';
                html += '<div class="col-sm-12 col-12 text-center">';
                html += '<h5 class="card-title">'+pesquisaVagas[i][0].title+'</h5>';
                // html += '<h6 class="card-subtitle mb-2 text-muted">Programador</h6>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: white;">';
                //html += '<input type="hidden" class="form-control form-control-lg" name="vagasID" id="vagasID" value="'+pesquisaVagas[i][0].candidates_id+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
                html += '<div class="col-sm-12 col-12" >';
                html += '<div class="row" >';
                html += '<div class="col-sm-12 col-12 mb-2">';
                html += '<span class="btn-success btn-sm">Quero me candidatar</span>';
                html += '</div>';
                html += '</br>';
                html += '<div class="col-sm-12 col-12" >';
                html += '<span class="btn-primary btn-sm" onclick="visualizarVaga('+pesquisaVagas[i][0].vacancies_id+')">Visualizar vaga</span>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }
            $("#divVagas").html(html);
        });
    }else{
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: "Campo obrigatorio",
            showConfirmButton: false,
            timer: 1500
        })
    }

});

function visualizarVaga(vacancies){
    var idVaga = vacancies;
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/visualizar/vagas",
        dataType: 'json',
        type: "post",
        data: {
            idVaga: idVaga
        },
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
          $('.page').removeClass('page-active');
          $("#telaDescricaoVaga").addClass('page-active');
          var tituloVaga = response.vaga[0].title;
          var descricaoVaga = response.vaga[0].description;
          var descricaoVaga = response.vaga[0].salary;
          var nomeEmpesa = response.nomeEmpresa;

          $("#tituloDaVagaSelecionada").html(tituloVaga);
          $("#nomeEmpresaVagaSelecionada").html(nomeEmpesa);
          $("#descricaoVagaSelecionada").html(descricaoVaga);
          $("#salarioVagaSelecionada").html(descricaoVaga);

        }
    });
}
  $("#btnPesquisarCandidatos").click(function(){
    let iddaVaga = $("#idDaVaga").val();
    let valor = $("#inputPesquisarCandidatos").val();
    let html = '';
    if(valor){
      $.ajax({
        url:"http://www.fecomerciomt.org.br/api/pesquisar/candidato",
        dataType: 'json',
        type: "post",
        data: {idVaga: $("#idDaVaga").val(), stringPesquisa: $("#inputPesquisarCandidatos").val()},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
          vagasCandidatos = response;
        }
      }).done(function(){
        $('.page').removeClass('page-active');
        $("#telaCandidatoVaga").addClass('page-active');
        html += '<input type="hidden" class="form-control form-control-lg" name="idDaVaga" id="idDaVaga" value="'+iddaVaga+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
          for (var i = 0; i < vagasCandidatos.length; i++) {
            html += '<div class="col-sm-12 col-12 pt-4 pb-2 pl-4 pr-4" id="candidato'+(i+1)+'">';
              html += '<div class="card text-center border-0">';
                html += '<div class="card-header text-muted text-right border-bottom-0" style="background-color: white;">';
                  html += '<p style="font-size: .6rem;">'+dataCadastroCandidatos(vagasCandidatos[i][0].created_at)+'</p>';
                html += '</div>';
                html += '<div class="card-body p-2">';
                  html += '<div class="row">';
                    html += '<div class="col-sm-4 col-4">';
                    if(vagasCandidatos[i][0].foto){
                      html += '<img class="card-img-top w-100 rounded-circle" src="http://www.fecomerciomt.org.br/storage/'+vagasCandidatos[i][0].foto+'" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem de capa do card">';
                    }else{
                      html += '<img class="card-img-top w-100 rounded-circle" src="./img/BannerTemporario.png" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem de capa do card">';
                    }
                      
                    html += '</div>';
                    html += '<div class="col-sm-8 col-8 text-left">';
                      html += '<h5 class="card-title">'+vagasCandidatos[i][0].name+'</h5>';
                      // html += '<h6 class="card-subtitle mb-2 text-muted">Programador</h6>';
                    html += '</div>';
                  html += '</div>';
                html += '</div>';
              html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: white;">';
                html += '<input type="hidden" class="form-control form-control-lg" name="vagasID" id="vagasID" value="'+vagasCandidatos[i][0].candidates_id+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
                html += '<span class="h-100 w-100" onclick="pdfCandidato('+i+')">PDF do candidato</span>';
              html += '</div>';
            html += '</div>';
            html += '</div>';
          }
          $("#divCandidatos").html(html);
      });
    }else{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: "Campo obrigatorio",
        showConfirmButton: false,
        timer: 1500
      })
    }
    
  });
  $( "#btnTalentosContratados" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnTalentosContratados').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnTelaPerfil" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        if(response.codgResponse == 200){
          $("#inputNomePerfil").val(sessionStorage.nome);
          $("#inputDocumentoPerfil").val(sessionStorage.cnpj);
          $("#inputEmailPerfil").val(sessionStorage.email);
          $("#inputAddressPerfil").val(sessionStorage.address);
          var target = $('#btnTelaPerfil').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnTelaTalento" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnTelaTalento').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardAlugarEspaco" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardAlugarEspaco').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackAlterarVagas" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackAlterarVagas').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardStatusAlugarEspacoPendente" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardStatusAlugarEspacoPendente').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardDescricaoSala" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardDescricaoSala').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDashboardDescricaoSala2" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDashboardDescricaoSala2').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
   $( "#btnBackTelaStatusAlugarEspacoPendente" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackTelaStatusAlugarEspacoPendente').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });

  
  $( "#btnTelaEstadoSolicitacaoSala" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnTelaEstadoSolicitacaoSala').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btntelaDescricaoSala" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btntelaDescricaoSala').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDescricaoSala" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDescricaoSala').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnBackDescricaoSala2" ).click(function() {
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#btnBackDescricaoSala2').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $( "#btnSolicitarReserva" ).click(function() {
      $.ajax({
        url:"http://www.fecomerciomt.org.br/api/solicitacao/datas",
        dataType: 'json',
        type: "post",
        data: {nome: "teste"},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
          
            for (var z = 0; z < response.length; z++){
                console.log(response[z])
                datasfe.push(response[z]);
            }
        }
    }).done(function(){
        class Calendar {
            constructor(id) {
                this.cells = [];
                this.datasSelecionadas = [];
                this.selectedDate = null;
                this.currentMonth = moment();
                this.elCalendar = document.getElementById(id);
                this.showTemplate();
                this.elGridBody = this.elCalendar.querySelector('.grid__body');
                this.elMonthName = this.elCalendar.querySelector('.month-name');
                this.showCells();
            }
        
            showTemplate() {
                this.elCalendar.innerHTML = this.getTemplate();
                this.addEventListenerToControls();
            }
        
            getTemplate() {
                let template = `
                    <div class="calendar__header">
                        <button type="button" class="control control--prev">&lt;</button>
                        <span class="month-name">dic 2019</span>
                        <button type="button" class="control control--next">&gt;</button>
                    </div>
                    <div class="calendar__body">
                        <div class="grid">
                            <div class="grid__header">
                                <span class="grid__cell grid__cell--gh">Seg</span>
                                <span class="grid__cell grid__cell--gh">Ter</span>
                                <span class="grid__cell grid__cell--gh">Qua</span>
                                <span class="grid__cell grid__cell--gh">Qui</span>
                                <span class="grid__cell grid__cell--gh">Sex</span>
                                <span class="grid__cell grid__cell--gh">Sáb</span>
                                <span class="grid__cell grid__cell--gh">Dom</span>
                            </div>
                            <div class="grid__body">
                            </div>
                        </div>
                    </div>
                `;
                return template;
            }
        
            addEventListenerToControls() {
                let elControls = this.elCalendar.querySelectorAll('.control');
                elControls.forEach(elControl => {
                    elControl.addEventListener('click', e => {
                        let elTarget = e.target;
                        if (elTarget.classList.contains('control--next')) {
                            this.changeMonth(true);
                        } else {
                            this.changeMonth(false);
                        }
                        this.showCells();
                    });
                });
            }
        
            changeMonth(next = true) {
                if (next) {
                    this.currentMonth.add(1, 'months');
                } else {
                    this.currentMonth.subtract(1, 'months');
                }
            }
        
            showCells() {
                this.cells = this.generateDates(this.currentMonth);
                if (this.cells === null) {
                    console.error('Não foi possível gerar datas do calendário.');
                    return;
                }
        
                this.elGridBody.innerHTML = '';
                let templateCells = '';
                let disabledClass = '';
                
                for (let i = 0; i < this.cells.length; i++) {
                    disabledClass = '';
                    if (!this.cells[i].isInCurrentMonth) {
                        disabledClass = 'grid__cell--disabled';
                    }
                    let dataMomento;
                    if(((this.cells[i].date.month()+1) < 10) && (this.cells[i].date.date()< 10)){
                        dataMomento = this.cells[i].date.year()+'-0'+(this.cells[i].date.month()+ 1)+'-0'+this.cells[i].date.date();
                    }
                    if(((this.cells[i].date.month()+1) >= 10) && (this.cells[i].date.date()< 10)){
                        dataMomento = this.cells[i].date.year()+'-'+(this.cells[i].date.month()+ 1)+'-0'+this.cells[i].date.date();
                    }
                    if(((this.cells[i].date.month()+1) < 10) && (this.cells[i].date.date()>= 10)){
                        dataMomento = this.cells[i].date.year()+'-0'+(this.cells[i].date.month()+ 1)+'-'+this.cells[i].date.date();
                    }
                    if(((this.cells[i].date.month()+1) >= 10) && (this.cells[i].date.date()>= 10)){
                        dataMomento = this.cells[i].date.year()+'-'+(this.cells[i].date.month()+ 1)+'-'+this.cells[i].date.date();
                    }
                    for (let index = 0; index < datasfe.length; index++) {
                        if(dataMomento == datasfe[index]){
                            disabledClass = 'grid__cell--disabled';
                        }
                    }
                    templateCells += `
                        <span class="grid__cell grid__cell--gd ${disabledClass}" data-cell-id="${i}">
                            ${this.cells[i].date.date()}
                        </span>
                    `;
                }
                this.elMonthName.innerHTML = this.currentMonth.format('MMM YYYY');
                this.elGridBody.innerHTML = templateCells;
                this.addEventListenerToCells();
            }
        
            generateDates(monthToShow = moment()) {
                if (!moment.isMoment(monthToShow)) {
                    return null;
                }
                let dateStart = moment(monthToShow).startOf('month');
                let dateEnd = moment(monthToShow).endOf('month');
                let cells = [];
        
                while (dateStart.day() !== 1) {
                    dateStart.subtract(1, 'days');
                }
        
                while (dateEnd.day() !== 0) {
                    dateEnd.add(1, 'days');
                }
        
                do {
                    cells.push({
                        date: moment(dateStart),
                        isInCurrentMonth: dateStart.month() === monthToShow.month()
                    });
                    dateStart.add(1, 'days');
                } while (dateStart.isSameOrBefore(dateEnd));
        
                return cells;
            }
        
            addEventListenerToCells() {
                let elCells = this.elCalendar.querySelectorAll('.grid__cell--gd');
                elCells.forEach(elCell => {
                    elCell.addEventListener('click', e => {
                        let elTarget = e.target;
                        let selectedCell = this.elGridBody.querySelector('.grid__cell--selected');
                        
                        if (elTarget.classList.contains('grid__cell--disabled') || elTarget.classList.contains('grid__cell--selected')) {
                            elTarget.classList.remove('grid__cell--selected');
                            this.selectedDate = this.cells[parseInt(elTarget.dataset.cellId)].date;
                            let dataRetirada = this.datasSelecionadas[this.datasSelecionadas.indexOf(this.cells[parseInt(elTarget.dataset.cellId)].date)].format('YYYY')+'-'+this.datasSelecionadas[this.datasSelecionadas.indexOf(this.cells[parseInt(elTarget.dataset.cellId)].date)].format('M')+'-'+this.datasSelecionadas[this.datasSelecionadas.indexOf(this.cells[parseInt(elTarget.dataset.cellId)].date)].format('D');                    
                            this.datasSelecionadas.splice(this.datasSelecionadas.indexOf(this.cells[parseInt(elTarget.dataset.cellId)].date), 1);
                            dataCalendarioP.splice(dataCalendarioP.indexOf(dataRetirada), 1);
                            
                            console.log("retirado");
                            console.log(dataCalendarioP);
                            return;
                        }                
                        elTarget.classList.add('grid__cell--selected');
                        this.selectedDate = this.cells[parseInt(elTarget.dataset.cellId)].date;
                        let ee = this.selectedDate;
                        if(elTarget.classList.contains('grid__cell--disabled')){

                        }else{
                          this.datasSelecionadas.push(ee);
                        }
                        
                        this.elCalendar.dispatchEvent(new Event('change'));
                        console.log("adidionando");
                        console.log(dataCalendarioP);
                    });
                });
            }
        
            getElement() {
                return this.elCalendar;
            }
        
            value() {
                return this.selectedDate;
            }
        }
        let calendar = new Calendar('calendar');
        calendar.getElement().addEventListener('change', e => {
            let calendario = calendar.value().format('YYYY')+'-'+calendar.value().format('M')+'-'+calendar.value().format('D');
            
            dataCalendarioP.push(calendario);
        });
        var target = $('#btnSolicitarReserva').attr('dt-page');
            $('.page').removeClass('page-active');
            $(target).addClass('page-active');
    });
  });
  $("#btnSolicitarReserva2").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/solicitacao/datas2",
      dataType: 'json',
      type: "post",
      data: {nome: "teste"},
      success: function(response){
          for (var z = 0; z < response.length; z++){
              console.log(response[z])
              datasfe2.push(response[z]);
              // var b = response;
          }
          // let daf = new Date(response[0]);
          // console.log(daf.getDate());
          // if (this.cells[i].date.date() == daf.getDate()) {
          //     disabledClass = 'grid__cell--disabled';
          // }
      }
  }).done(function(){
      class Calendar2 {
          constructor(id) {
              this.cells = [];
              this.datasSelecionadas = [];
              this.selectedDate = null;
              this.currentMonth = moment();
              this.elCalendar = document.getElementById(id);
              this.showTemplate();
              this.elGridBody = this.elCalendar.querySelector('.grid__body');
              this.elMonthName = this.elCalendar.querySelector('.month-name');
              this.showCells();
          }
      
          showTemplate() {
              this.elCalendar.innerHTML = this.getTemplate();
              this.addEventListenerToControls();
          }
      
          getTemplate() {
              let template = `
                  <div class="calendar__header">
                      <button type="button" class="control control--prev">&lt;</button>
                      <span class="month-name">dic 2019</span>
                      <button type="button" class="control control--next">&gt;</button>
                  </div>
                  <div class="calendar__body">
                      <div class="grid">
                          <div class="grid__header">
                              <span class="grid__cell grid__cell--gh">Seg</span>
                              <span class="grid__cell grid__cell--gh">Ter</span>
                              <span class="grid__cell grid__cell--gh">Qua</span>
                              <span class="grid__cell grid__cell--gh">Qui</span>
                              <span class="grid__cell grid__cell--gh">Sex</span>
                              <span class="grid__cell grid__cell--gh">Sáb</span>
                              <span class="grid__cell grid__cell--gh">Dom</span>
                          </div>
                          <div class="grid__body">
                          </div>
                      </div>
                  </div>
              `;
              return template;
          }
      
          addEventListenerToControls() {
              let elControls = this.elCalendar.querySelectorAll('.control');
              elControls.forEach(elControl => {
                  elControl.addEventListener('click', e => {
                      let elTarget = e.target;
                      if (elTarget.classList.contains('control--next')) {
                          this.changeMonth(true);
                      } else {
                          this.changeMonth(false);
                      }
                      this.showCells();
                  });
              });
          }
      
          changeMonth(next = true) {
              if (next) {
                  this.currentMonth.add(1, 'months');
              } else {
                  this.currentMonth.subtract(1, 'months');
              }
          }
      
          showCells() {
              this.cells = this.generateDates(this.currentMonth);
              if (this.cells === null) {
                  console.error('Não foi possível gerar datas do calendário.');
                  return;
              }
      
              this.elGridBody.innerHTML = '';
              let templateCells = '';
              let disabledClass = '';
              // let d = new Date();
              
              for (let i = 0; i < this.cells.length; i++) {
                  disabledClass = '';
                  if (!this.cells[i].isInCurrentMonth) {
                      disabledClass = 'grid__cell--disabled';
                  }
                  let dataMomento;
                  if(((this.cells[i].date.month()+1) < 10) && (this.cells[i].date.date()< 10)){
                      dataMomento = this.cells[i].date.year()+'-0'+(this.cells[i].date.month()+ 1)+'-0'+this.cells[i].date.date();
                  }
                  if(((this.cells[i].date.month()+1) >= 10) && (this.cells[i].date.date()< 10)){
                      dataMomento = this.cells[i].date.year()+'-'+(this.cells[i].date.month()+ 1)+'-0'+this.cells[i].date.date();
                  }
                  if(((this.cells[i].date.month()+1) < 10) && (this.cells[i].date.date()>= 10)){
                      dataMomento = this.cells[i].date.year()+'-0'+(this.cells[i].date.month()+ 1)+'-'+this.cells[i].date.date();
                  }
                  if(((this.cells[i].date.month()+1) >= 10) && (this.cells[i].date.date()>= 10)){
                      dataMomento = this.cells[i].date.year()+'-'+(this.cells[i].date.month()+ 1)+'-'+this.cells[i].date.date();
                  }
                  for (let index = 0; index < datasfe2.length; index++) {
                      if(dataMomento == datasfe2[index]){
                          disabledClass = 'grid__cell--disabled';
                      }
                  }
                  // console.log(b);
                  // if(this.cells[i].date.date() == d.getDay()){
                  
                  // }
                  // <span class="grid__cell grid__cell--gd grid__cell--selected">1</span>
                  templateCells += `
                      <span class="grid__cell grid__cell--gd ${disabledClass}" data-cell-id="${i}">
                          ${this.cells[i].date.date()}
                      </span>
                  `;
              }
              this.elMonthName.innerHTML = this.currentMonth.format('MMM YYYY');
              this.elGridBody.innerHTML = templateCells;
              this.addEventListenerToCells();
          }
      
          generateDates(monthToShow = moment()) {
              if (!moment.isMoment(monthToShow)) {
                  return null;
              }
              let dateStart = moment(monthToShow).startOf('month');
              let dateEnd = moment(monthToShow).endOf('month');
              let cells = [];
      
              // Encontre a primeira data a ser exibida no calendário
              while (dateStart.day() !== 1) {
                  dateStart.subtract(1, 'days');
              }
      
              // Encontre a última data a ser exibida no calendário
              while (dateEnd.day() !== 0) {
                  dateEnd.add(1, 'days');
              }
      
              // Gere as datas da grade
              do {
                  cells.push({
                      date: moment(dateStart),
                      isInCurrentMonth: dateStart.month() === monthToShow.month()
                  });
                  dateStart.add(1, 'days');
              } while (dateStart.isSameOrBefore(dateEnd));
      
              return cells;
          }
      
          addEventListenerToCells() {
              let elCells = this.elCalendar.querySelectorAll('.grid__cell--gd');
              elCells.forEach(elCell => {
                  elCell.addEventListener('click', e => {
                      let elTarget = e.target;
                      let selectedCell = this.elGridBody.querySelector('.grid__cell--selected');
                      if (elTarget.classList.contains('grid__cell--disabled') || elTarget.classList.contains('grid__cell--selected')) {
                          elTarget.classList.remove('grid__cell--selected');
                          this.selectedDate = this.cells[parseInt(elTarget.dataset.cellId)].date;
                          let dataRetirada = this.datasSelecionadas[this.datasSelecionadas.indexOf(this.cells[parseInt(elTarget.dataset.cellId)].date)].format('YYYY')+'-'+this.datasSelecionadas[this.datasSelecionadas.indexOf(this.cells[parseInt(elTarget.dataset.cellId)].date)].format('M')+'-'+this.datasSelecionadas[this.datasSelecionadas.indexOf(this.cells[parseInt(elTarget.dataset.cellId)].date)].format('D');                    
                          this.datasSelecionadas.splice(this.datasSelecionadas.indexOf(this.cells[parseInt(elTarget.dataset.cellId)].date), 1);
                          data2.splice(data2.indexOf(dataRetirada), 1);
                          //TESTES
                          console.log("retirado");
                          console.log(data2);
                          return;
                      }                
                      elTarget.classList.add('grid__cell--selected');
                      this.selectedDate = this.cells[parseInt(elTarget.dataset.cellId)].date;
                      let ee = this.selectedDate;
                      if(elTarget.classList.contains('grid__cell--disabled')){

                      }else{
                        this.datasSelecionadas.push(ee);
                      }
                      this.elCalendar.dispatchEvent(new Event('change'));
                      // TESTES
                      console.log("adidionando");
                      console.log(data2);
                  });
              });
          }
      
          getElement() {
              return this.elCalendar;
          }
      
          value() {
              return this.selectedDate;
          }
      }
      let calendar2 = new Calendar2('calendar2');
      calendar2.getElement().addEventListener('change', e => {
          let calendario2 = calendar2.value().format('YYYY')+'-'+calendar2.value().format('M')+'-'+calendar2.value().format('D');
          
          data2.push(calendario2);
      });
      var target = $('#btnSolicitarReserva2').attr('dt-page');
      $('.page').removeClass('page-active');
      $(target).addClass('page-active');
    });
  });
  $("#bolaCertificadoDigital").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/cidades",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      success: function(response){
        var myselect = $('#CertificadoDigitalCidades');
        myselect.append('<option value="">Selecione uma cidade</option>');
        $.each(response.cidades, function(index, key) {
          myselect.append( $('<option></option>').val(key.cities_id).html(key.name));
        });
        myselect.append(myselect.html());
      }
    });
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#bolaCertificadoDigital').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  
  $("#bolaCertificadoOrigem").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/cidades",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      success: function(response){
        var myselect = $('#CertificadoOrigemCidades');
        myselect.append('<option value="">Selecione uma cidade</option>');
        $.each(response.cidades, function(index, key) {
          myselect.append( $('<option></option>').val(key.cities_id).html(key.name));
        });
        myselect.append(myselect.html());
      }
    });
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#bolaCertificadoOrigem').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
          
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $("#bolaContribuicoes").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/cidades",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      success: function(response){
        var myselect = $('#ContribuicoesCidades');
        myselect.append('<option value="">Selecione uma cidade</option>');
        $.each(response.cidades, function(index, key) {
          myselect.append( $('<option></option>').val(key.cities_id).html(key.name));
        });
        myselect.append(myselect.html());
      }
    });
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#bolaContribuicoes').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });

  $("#bolaDuvidaSugestoes").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#bolaDuvidaSugestoes').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $("#bolaVoucher").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#bolaVoucher').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $("#bolaBancoTalento").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/bancoDeTalentos",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token, id: sessionStorage.companies_id},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        if(response.codgResponse == 200){
          if (sessionStorage.tipoDocumento == 1){
              $('.page').removeClass('page-active');
              $('#telaPesquisaDeVaga').addClass('page-active');
          }
          if (sessionStorage.tipoDocumento == 2) {
              var target = $('#bolaBancoTalento').attr('dt-page');
              $('.page').removeClass('page-active');
              $(target).addClass('page-active');

              if(response.numrVagas < 10){
                  $("#contagemVagas").html("0"+response.numrVagas);
              }else{
                  $("#contagemVagas").html(response.numrVagas);
              }
              if(response.soma < 10){
                  $("#contagemCurriculo").html("0"+response.soma);
              }else{
                  $("#contagemCurriculo").html(response.soma);
              }
          }
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  $("#bolaCartaoEmpresarial").click(function(){
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/cidades",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      success: function(response){
        var myselect = $('#inputMunicipioCE');
        var myselect2 = $('#inputCidadeEmpresaCE');
        myselect.append('<option value="">Selecione uma cidade</option>');
        $.each(response.cidades, function(index, key) {
          myselect.append( $('<option></option>').val(key.cities_id).html(key.name));
        });
        myselect.append(myselect.html());

        
        myselect2.append('<option value="">Selecione uma cidade</option>');
        $.each(response.cidades, function(index, key) {
          myselect2.append( $('<option></option>').val(key.cities_id).html(key.name));
        });
        myselect2.append(myselect2.html());
      }
    });
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/paises2",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      success: function(response){
        var myselect = $('#inputUFCE');
        myselect.append('<option value="">Selecione uma cidade</option>');
        $.each(response.countries, function(index, key) {
          myselect.append( $('<option></option>').val(key.states_id).html(key.name));
        });
        myselect.append(myselect.html());
      }
    });
    $.ajax({
      url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
      dataType: 'json',
      type: "post",
      data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
      beforeSend: function() {
        $('.page').removeClass('page-active');
        $("#telaLoading").addClass('page-active'); 
      },
      success: function(response){
        
        if(response.codgResponse == 200){
          var target = $('#bolaCartaoEmpresarial').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          })
          sessionStorage.clear();
          var target = $('#btnTelaLogin').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
        }
      }
    });
  });
  

$( "#btnBackDashboardTelaPesquisaDeVagas" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){

            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardTelaPesquisaDeVagas').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});

$("#bolaCalendarioEvento").click(function(){
    var html = '';
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/eventos",
        dataType: 'json',
        type: "post",
        data: {cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
            eventos = response;
        }
        }).done(function(){
          var target = $('#bolaCalendarioEvento').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
            for (var i = 0; i < eventos.eventos.length; i++) {
                html += '<div class="col-sm-12 col-12 pt-4 pb-2 pl-4 pr-4" id="evento'+(i+1)+'">';
                html += '<div class="card text-center border-0">';
                html += '<div class="card-header text-muted text-right border-bottom-0" style="background-color: white;">';
                html += '<p style="font-size: .6rem;">Local: '+eventos.eventos[i].local+'</p>';
                html += '</div>';
                html += '<div class="card-body p-2">';
                html += '<div class="row">';
                html += '<div class="col-sm-4 col-4">';
                if(eventos.eventos[i].image){
                    html += '<img class="card-img-top w-100 rounded-circle" src="http://www.fecomerciomt.org.br/storage/'+eventos.eventos[i].image+'" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem de capa do card">';
                }else{
                    html += '<img class="card-img-top w-100 rounded-circle" src="./img/BannerTemporario.png" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem de capa do card">';
                }

                html += '</div>';
                html += '<div class="col-sm-8 col-8 text-left">';
                html += '<h5 class="card-title">'+eventos.eventos[i].title+'</h5>';
                // html += '<h6 class="card-subtitle mb-2 text-muted">Programador</h6>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: white;">';
                html += '<input type="hidden" class="form-control form-control-lg" name="vagasID" id="vagasID" value="'+eventos.eventos[i].events_id+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
                html += '<span class="h-100 w-100" onclick="saibaMaisEvento('+i+')">Saiba mais</span>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }
            $("#divEventos").html(html);
    });
});

function saibaMaisEvento(evento) {
    $('.page').removeClass('page-active');
    $('#telaSaibaMaisEvento').addClass('page-active');
    $('#tituloEvento').html(eventos.eventos[evento].title);
    $('#datasEvento').html("De "+dataFormartEvento(eventos.eventos[evento].date_start)+" Até "+dataFormartEvento(eventos.eventos[evento].date_end));
    $('#imagemDoEvento').html('<img class="card-img-top w-100" src="http://www.fecomerciomt.org.br/storage/'+eventos.eventos[evento].image+'" style="object-fit: cover;object-position: center;" alt="Imagem de capa do card">');
    $('#textoDoEvento').html(eventos.eventos[evento].text);
}

$( "#btnBackDashboardSaibaMaisEvento" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){

            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardSaibaMaisEvento').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});

$( "#btnBackDashboardCalendarioDeEventos" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){

            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardCalendarioDeEventos').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});

$("#bolaPesquisa").click(function(){
    var html = '';
    

    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/pesquisas",
        dataType: 'json',
        type: "post",
        data: {cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
            noticias = response;
        }
    }).done(function(){
      var target = $('#bolaPesquisa').attr('dt-page');
    $('.page').removeClass('page-active');
    $(target).addClass('page-active');
        for (var i = 0; i < noticias.noticias.length; i++) {
            html += '<div class="col-sm-12 col-12 pt-4 pb-2 pl-4 pr-4" id="pesquisa'+(i+1)+'">';
            html += '<div class="card text-center border-0">';
            html += '<div class="card-header text-muted text-right border-bottom-0" style="background-color: white;">';
            html += '<p style="font-size: .6rem;">Data de cadastro: '+dataFormartEvento(noticias.noticias[i].date_at)+'</p>';
            html += '</div>';
            html += '<div class="card-body p-2">';
            html += '<div class="row">';
            html += '<div class="col-sm-4 col-4">';
            if(noticias.noticias[i].image){
                html += '<img class="card-img-top w-100 rounded-circle" src="http://www.fecomerciomt.org.br/storage/'+noticias.noticias[i].image+'" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem da pesquisa">';
            }else{
                html += '<img class="card-img-top w-100 rounded-circle" src="./img/BannerTemporario.png" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem de capa do card">';
            }

            html += '</div>';
            html += '<div class="col-sm-8 col-8 text-left">';
            html += '<h5 class="card-title">'+noticias.noticias[i].title+'</h5>';
            // html += '<h6 class="card-subtitle mb-2 text-muted">Programador</h6>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: white;">';
            html += '<input type="hidden" class="form-control form-control-lg" name="vagasID" id="vagasID" value="'+noticias.noticias[i].news_id+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
            html += '<span class="h-100 w-100" onclick="saibaMaisPesquisa('+i+')">Saiba mais</span>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }
        $("#divPesquisas").html(html);
    });
});

function saibaMaisPesquisa(noticia) {
    $('.page').removeClass('page-active');
    $('#telaSaibaMaisPesquisa').addClass('page-active');
    $('#tituloPesquisa').html(noticias.noticias[noticia].title);
    $('#datasPesquisa').html("Data de cadastro "+dataFormartEvento(noticias.noticias[noticia].date_at));
    $('#imagemDaPesquisa').html('<img class="card-img-top w-100" src="http://www.fecomerciomt.org.br/storage/'+noticias.noticias[noticia].image+'" style="object-fit: cover;object-position: center;" alt="Imagem de capa do card">');
    $('#textoDaPesquisa').html(noticias.noticias[noticia].text);
}

$( "#btnBackDashboardPesquisa" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){

            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardPesquisa').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});

$( "#btnBackDashboardSaibaMaisPesquisa" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){

            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardSaibaMaisPesquisa').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});

$("#bolaConvenios").click(function(){
    var html = '';
    

    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/convenios",
        dataType: 'json',
        type: "post",
        data: {cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
            convenios = response;
        }
    }).done(function(){
        // limitador de caracter limitadorCaractere
        var target = $('#bolaConvenios').attr('dt-page');
        $('.page').removeClass('page-active');
        $(target).addClass('page-active');
       for (var i = 0; i < convenios.convenios.length; i++) {
            html += '<div class="row p-3">';
            html += '<div class="card">';
            html += '<div class="card-body p-3">';
            html += '<div class="row">';
            html += '<div class="col-sm-4 col-4">';
            html += '<img class="card-img-top w-100" src="http://www.fecomerciomt.org.br/storage/'+convenios.convenios[i].image+'" style="height: 82px;" alt="Imagem do convenio">';
            html += '</div>';
            html += '<div class="col-sm-6 col-6">';
            html += '<h5 class="card-title">'+convenios.convenios[i].title+'</h5>';
            html += '<h6 class="card-subtitle mb-2 text-muted">'+convenios.convenios[i].text+'</h6>';
            //html += '<h6 class="card-subtitle mb-2 text-muted pt-2" style="font-size: .6rem;">(99)9 9999-9999</h6>';
            //html += '<h6 class="card-subtitle mb-2 text-muted pt-1 limitadorCaractere" style="font-size: .6rem;">endereço, bairro, Cuiabá-MT</h6>';
            html += '</div>';
            html += '<div class="col-sm-2 col-2 text-right">';
            html += '<i class="zmdi zmdi-more"></i>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }
        $("#divConvenios").html(html);
    });
});

$("#bolaDadosMt").click(function(){
    var html = '';
    

    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/dadosMT",
        dataType: 'json',
        type: "post",
        data: {cnpj: sessionStorage.cnpj, token: sessionStorage.token },
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
            noticias = response;
        }
    }).done(function(){
        var target = $('#bolaDadosMt').attr('dt-page');
        $('.page').removeClass('page-active');
        $(target).addClass('page-active');
        for (var i = 0; i < noticias.noticias.length; i++) {
            html += '<div class="col-sm-12 col-12 pt-4 pb-2 pl-4 pr-4" id="pesquisa'+(i+1)+'">';
            html += '<div class="card text-center border-0">';
            html += '<div class="card-header text-muted text-right border-bottom-0" style="background-color: white;">';
            html += '<p style="font-size: .6rem;">Data de cadastro: '+dataFormartEvento(noticias.noticias[i].date_at)+'</p>';
            html += '</div>';
            html += '<div class="card-body p-2">';
            html += '<div class="row">';
            // html += '<div class="col-sm-4 col-4">';
            // if(noticias.noticias[i].image){
            //     html += '<img class="card-img-top w-100 rounded-circle" src="http://www.fecomerciomt.org.br/storage/'+noticias.noticias[i].image+'" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem da pesquisa">';
            // }else{
            //     html += '<img class="card-img-top w-100 rounded-circle" src="./img/BannerTemporario.png" style="height: 82px;object-fit: cover;object-position: center;" alt="Imagem de capa do card">';
            // }

            // html += '</div>';
            html += '<div class="col-sm-12 col-12 text-center">';
            html += '<h5 class="card-title">'+noticias.noticias[i].title+'</h5>';
            // html += '<h6 class="card-subtitle mb-2 text-muted">Programador</h6>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="card-footer text-muted border-top-0 text-center" style="background-color: white;">';
            html += '<input type="hidden" class="form-control form-control-lg" name="vagasID" id="vagasID" value="'+noticias.noticias[i].renalegisposts_id+'" style="border-top: none;border-left: none;border-right: none;border-radius: 0px;" required></input>';
            html += '<span class="h-100 w-100" onclick="saibaMaisDadosMt('+i+')">Saiba mais</span>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }
        $("#divDadosMt").html(html);
    });
});

function saibaMaisDadosMt(noticia) {
    $('.page').removeClass('page-active');
    $('#telaSaibaMaisDadosMt').addClass('page-active');
    $('#tituloDadosMt').html(noticias.noticias[noticia].title);
    $('#datasDadosMt').html("Data de cadastro "+dataFormartEvento(noticias.noticias[noticia].date_at));
    // $('#imagemDadosMt').html('<img class="card-img-top w-100" src="http://www.fecomerciomt.org.br/storage/'+noticias.noticias[noticia].image+'" style="object-fit: cover;object-position: center;" alt="Imagem de capa do card">');
    $('#textoDadosMt').html(noticias.noticias[noticia].text);
}

$( "#btnBackDashboardDadosMt" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardDadosMt').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});

$( "#btnBackDashboardSaibaMaisDadosMt" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){

            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardSaibaMaisDadosMt').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});
         
$( "#btntelaStatusAlugarEspacoPendente" ).click(function() {
  var html = '';
  
        $.ajax({
          url:"http://www.fecomerciomt.org.br/api/listar/solicitacoes",
          dataType: 'json',
          type: "post",
          data: {documento: sessionStorage.cnpj, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
          beforeSend: function() {
            $('.page').removeClass('page-active');
            $("#telaLoading").addClass('page-active'); 
          },
          success: function(response){
            solicitacoes = response;
          }
        }).done(function(){
          var target = $('#btntelaStatusAlugarEspacoPendente').attr('dt-page');
          $('.page').removeClass('page-active');
          $(target).addClass('page-active');
            for (var i = 0; i < solicitacoes.length; i++) {
              html += '<div class="card border-0" id="solicitacaoReserva_'+solicitacoes[i].rentalrequests_id+'">';
                html += '<div class="card-body">';
                  html += '<div class="row">';
                    html += '<div class="col-sm-8 col-8">';
                    if(solicitacoes[i].rentals_id == 1){
                      html += '<h5 class="card-title">Auditório</h5>';
                    }
                    if(solicitacoes[i].rentals_id == 2){
                      html += '<h5 class="card-title">Plenário</h5>';
                    }
                      html += '<p class="card-text">'+solicitacoes[i].reserved_dates+'</p>';
                    html += '</div>';
                    html += '<div class="col-sm-4 col-4 w-100 p-0 text-center">';
                    if(solicitacoes[i].status == 1){
                      html += '<a href="#" class="btn btn-success w-100 h-100 text-center rounded-0 p-3">Reservado</a>';
                    }
                    if(solicitacoes[i].status == 2){
                      html += '<a href="#" class="btn btn-primary w-100 h-100 text-center rounded-0 p-3 botaoCorFecomercioPrimario">Pendente</a>';
                    }
                    if(solicitacoes[i].status == 3){
                      html += '<a href="#" class="btn btn-danger w-100 h-100 text-center rounded-0 p-3">Cancelado</a>';
                    }
                    html += '</div>';
                  html += '</div>';
                html += '</div>';
              html += '</div>';
            }
            $("#divReservasPendentes").html(html);
        });
});

function testereservaSala(index){
  if(index == 0){
    $('.page').removeClass('page-active');
    $("#telaDescricaoSala2").addClass('page-active');
    $("#tituloSalaDescricao2").html(salas[index].title);
    $("#divDescricaoSalaCaracteristicas2").html(salas[index].text);
    $("#divTextoSalaDescricao2").html(salas[index].text_request);
  }
  if(index == 1){
    $('.page').removeClass('page-active');
    $("#telaDescricaoSala").addClass('page-active');
    $("#tituloSalaDescricao").html(salas[index].title);
    $("#divDescricaoSalaCaracteristicas").html(salas[index].text);
    $("#divTextoSalaDescricao").html(salas[index].text_request);
  }
  
  
}
$("#bolaAlugarEspaco").click(function(){
  var html = '';
  

  $.ajax({
    url:"http://www.fecomerciomt.org.br/api/listar/salas",
    dataType: 'json',
    type: "post",
    data: {documento: sessionStorage.cnpj,cnpj: sessionStorage.cnpj, token: sessionStorage.token},
    beforeSend: function() {
      $('.page').removeClass('page-active');
      $("#telaLoading").addClass('page-active'); 
    },
    success: function(response){
      salas = response;
    }
  }).done(function(){
    var target = $('#bolaAlugarEspaco').attr('dt-page');
  $('.page').removeClass('page-active');
  $(target).addClass('page-active');
      for (var i = 0; i < salas.length; i++) {
          html += '<div class="card mb-2" id="salaReserva'+salas[i].rentals_id+'">';
            html += '<div class="bolaAluguel text-center" onclick="testereservaSala('+i+')"><i class="zmdi zmdi-plus zmdi-hc-2x" style="color:white;"></i></div>';
            html += '<img class="card-img-top" src="./img/BannerTemporario.png" alt="Imagem de capa do card">';
            html += '<div class="card-body">';
              html += '<div class="row">';
                html += '<div class="col-sm-8 col-8">';
                  html += '<h5 class="card-title">'+salas[i].title+'</h5>';
                  html += '<p class="card-text">Selecione e escolha uma data</p>';
                html += '</div>';
                html += '<div class="col-sm-4 col-4 text-left pr-0">';
                  html += '<i class="zmdi zmdi-accounts zmdi-hc-2x pt-3" style="color:#b28706;"></i><span class="pl-1">+'+salas[i].capacity+'</span>';
                html += '</div>';
              html += '</div>';
            html += '</div>';
          html += '</div>';
      }
      $("#divSalasReserva").html(html);
  });
});

$("#bolaConvencoes").click(function(){
    var html = '';
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/convencoes",
        dataType: 'json',
        type: "post",
        data: {documento: sessionStorage.cnpj, cnpj: sessionStorage.cnpj, token: sessionStorage.token },
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){
            convencoesPdf = response;
        }
    }).done(function(){
        var target = $('#bolaConvencoes').attr('dt-page');
        $('.page').removeClass('page-active');
        $(target).addClass('page-active');
        for (var i = 0; i < convencoesPdf.convencoes.length; i++) {
            html += '<div class="accordion" id="accordionExample'+convencoesPdf.convencoes[i].conventions_id+'">';
            html += '<div class="card">';
            html += '<div class="card-header" id="heading'+convencoesPdf.convencoes[i].conventions_id+'">';
            html += '<h2 class="mb-0">';
            html += '<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse'+convencoesPdf.convencoes[i].conventions_id+'" aria-expanded="true" aria-controls="collapse'+convencoesPdf.convencoes[i].conventions_id+'">'+convencoesPdf.convencoes[i].title+'</button>';
            html += '</h2>';
            html += '</div>';
            html += '<div id="collapse'+convencoesPdf.convencoes[i].conventions_id+'" class="collapse" aria-labelledby="heading'+convencoesPdf.convencoes[i].conventions_id+'" data-parent="#accordionExample'+convencoesPdf.convencoes[i].conventions_id+'">';
            html += '<div class="card-body">';
            for (var j = 0; j < convencoesPdf.arrayPDF.length; j++){
                if (convencoesPdf.arrayPDF[j].entity_id == convencoesPdf.convencoes[i].conventions_id ) {
                    html += '<p>'+convencoesPdf.arrayPDF[j].title+' <a href="http://www.fecomerciomt.org.br/storage/'+convencoesPdf.arrayPDF[j].path+'" target="_blank">Aqui</a></p>';
                }
            }
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        }
        $("#collapsePdf").html(html);
    });
});


$( "#btnBackDashboardConvencoes" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){

            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardConvencoes').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});

$( "#btnBackDashboardCadastroVagas" ).click(function() {
    $.ajax({
        url:"http://www.fecomerciomt.org.br/api/autenticationFMT",
        dataType: 'json',
        type: "post",
        data: {nome:sessionStorage.nome, cnpj: sessionStorage.cnpj, token: sessionStorage.token},
        beforeSend: function() {
          $('.page').removeClass('page-active');
          $("#telaLoading").addClass('page-active'); 
        },
        success: function(response){

            if(response.codgResponse == 200){
                var target = $('#btnBackDashboardCadastroVagas').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }else{
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                sessionStorage.clear();
                var target = $('#btnTelaLogin').attr('dt-page');
                $('.page').removeClass('page-active');
                $(target).addClass('page-active');
            }
        }
    });
});